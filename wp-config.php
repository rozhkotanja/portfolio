<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('WP_CACHE', true);
define( 'WPCACHEHOME', 'D:\works\programing\program\OpenServer\OSPanel\domains\jd\wp-content\plugins\wp-super-cache/' );
define( 'DB_NAME', 'jdtemplate' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'root' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', '' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'W*a|?wsNgKnV(~zbmQahgRO-,_6*XoAdQWb6Mr<!J.ZcJS/%R(Xj|5+/qA].pf]T');
define('SECURE_AUTH_KEY',  '9Bv:}6baZt5,Skd_D]*?VQN}-XZN]{#M ;z(5)}w6oGD+V^O*Z;0pidPV.]Av9+!');
define('LOGGED_IN_KEY',    'emkG2/OeNb=G,>WBv~L(GQ6)r1>:cw9V rNc)MP}=P`=@hFe$n+k_`UN{1sXN$.x');
define('NONCE_KEY',        'U !rs$n2<@laL$%5p6jPErEjYaB!d8)H@cCzLT&c<+M[q+&IMn|J?N Qwm++vq{I');
define('AUTH_SALT',        '<j,i>kdEg,-f~+TH_zSb80.kFNWGi22b$O%K+*:(*LIS|;+<0`Lmk-kXxnHAtl+>');
define('SECURE_AUTH_SALT', 'I6urannFN}7Qw-2U*]!.C2lrw4pJ@aNyj_W->Whq^j^6K|1ofWFvspmK7*}-=Z:?');
define('LOGGED_IN_SALT',   'p.e}MA=wzrQe4@ om-7@}+1:^{zS{vZ5n3:n~e@|]hvBVS3[|_Az#b)lX[KU]h+K');
define('NONCE_SALT',       'E[a@#RU<%tcT=J|,hPP/Mm,#H5Y&?<A@fSb?C$}=xi`3S.rOq7,Z%%U&0?~RBnv5');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}
define( 'WPCF7_AUTOP', false );
/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
