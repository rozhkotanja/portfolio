<?php

get_header();

?>
<!-- Blog Section Start -->
<section id="blog" class="section pt--100 pb--40">
    <div class="container">
        <div class="row">
            <div class="col-md-8 pb--60">
                <!-- Post Items Start -->
                <div class="post--items">

                    <?php
                    if (have_posts()) {
                        /* Start the Loop */
                        while (have_posts()) :
                            the_post();
                            get_template_part('template-parts/content/preview');
                        endwhile;

                        $paginate = paginate_links(['type' => 'array',
                            'prev_text' => '<',
                            'next_text' => '>']);
                        if (!empty($paginate)) {
                            ?>
                            <nav class="pagination--nav pt--50">
                                <ul class="pagination clearfix">
                                    <?php
                                    foreach ($paginate as $link) {
                                        $class = '';
                                        if (strpos($link, 'current') !== false) {
                                            $class = 'active';
                                        }
                                        echo "<li class = $class >$link</li>";
                                    }
                                    ?>
                                </ul>
                            </nav>
                            <?php
                        }
                    } else  echo "<h2>".__('Нічого не знайдено')."</h2>";
                    ?>

                </div>
            </div>

            <?php include_once 'sidebar.php'; ?>

        </div>
    </div>
</section>
<?php get_footer(); ?>

