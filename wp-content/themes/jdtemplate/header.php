<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri();?>/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri();?>/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri();?>/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri();?>/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri();?>/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri();?>/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri();?>/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri();?>/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri();?>/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri();?>/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri();?>/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri();?>/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri();?>/favicons/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_template_directory_uri();?>/favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri();?>/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- ==== Document Title ==== -->
    <title><?= get_the_title(); ?></title>
    
    <!-- ==== Document Meta ==== -->
    <meta name="author" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">

   
    <?php wp_head(); ?>
</head>


<body data-spy="scroll" data-target="#headerNav" data-offset="100" <?php body_class(); ?>>

    <!-- Preloader Start -->
    <div id="preloader" class="bg-primary">
        <div class="preloader--spinner"></div>
    </div>
    <!-- Preloader End -->

    <!-- Wrapper Start -->
    <div class="wrapper">
        <!-- Header Section Start -->
        <header id="header" class="header--section">
            <!-- Header Navbar Start -->
            <nav class="header--navbar navbar">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" 
                            class="navbar-toggle collapsed" 
                            data-toggle="collapse" 
                            data-target="#headerNav">
                                <span class="sr-only">Toggle Navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                        </button>

                        <!-- Logo Start -->
                        <a href="<?php echo home_url();   ?>" class="navbar-brand">
                            <span class="vc--parent">
                                <span class="vc--child">
                                    <img src="<?php echo get_field('site_logo','option')['url'];?>" style="max-height: 64px;" alt="">
                                </span>
                            </span>
                        </a>
                        <!-- Logo End -->
                    </div>

                    <div id="headerNav" class="navbar-collapse collapse">
                        <!-- Header Nav Links Start -->
                        <!-- <ul class="header--nav-links ff--primary nav navbar-right"> -->

                        <?php wp_nav_menu( [
                            'theme_location'  => 'top',
                            'menu'            => '', 
                            'container'       => 'div', 
                            'container_class' => '', 
                            'container_id'    => '',
                            'menu_class'      => 'menu', 
                            'menu_id'         => '',
                            'echo'            => true,
                            'fallback_cb'     => 'wp_page_menu',
                            'before'          => '',
                            'after'           => '',
                            'link_before'     => '',
                            'link_after'      => '',
                            'items_wrap'      => '<ul id="%1$s" class="header--nav-links ff--primary nav navbar-right  %2$s"> %3$s </ul>',
                            'depth'           => 0,
                            'walker'          => new Walker_Quickstart_Menu(), //use our custom walker
                            
                        ] );?>

                            <!-- <li class="active">
                                <a href="#header" data-trigger="scroll">
                                    Home
                                </a>
                            </li> -->


                            <!-- <li><a href="#aboutme" data-trigger="scroll">About Me</a></li>
                            <li><a href="#services" data-trigger="scroll">Services</a></li>
                            <li><a href="#portfolio" data-trigger="scroll">Portfolio</a></li>
                            <li><a href="#skills" data-trigger="scroll">Skills</a></li>
                            <li><a href="#blog" data-trigger="scroll">Blog</a></li>
                            <li><a href="#contact" data-trigger="scroll">Contact</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pages<span class="caret"></span></a>

                                <ul class="dropdown-menu">
                                    <li><a href="index.html">Home Page</a></li>
                                    <li><a href="blog.html">Blog Page</a></li>
                                    <li><a href="blog-details.html">Blog Details Page</a></li>
                                    <li><a href="contact.html">Contact Page</a></li>
                                    <li><a href="404.html">404 Page</a></li>
                                    <li><a href="coming-soon.html">Coming Soon Page</a></li>
                                </ul>
                            </li> -->
                        <!-- </ul> -->
                        <!-- Header Nav Links End -->
                    </div>
                </div>
            </nav>
            <!-- Header Navbar End -->
        </header>
        <!-- Header Section End -->
