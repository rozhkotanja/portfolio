<?php

/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();

?>
<!-- Blog Section Start -->
<section id="blog" class="section pt--100 pb--40">
    <div class="container">
        <div class="row">
            <div class="col-md-8 pb--60">
                <!-- Post Items Start -->

                <?php
                global $wp_query;
                $wp_query_params = [
                    "posts_per_page" => $wp_query->query_vars["posts_per_page"],
                    "post_type" => $wp_query->query_vars["post_type"],
                    "paged" => $wp_query->query_vars["paged"] ? $wp_query->query_vars["paged"] : 1,
                    "taxonomy" => $wp_query->query_var["taxonomy"],
                    "term" => $wp_query->query_var["term"]
                ];
                ?>
                <script>
                    var PAGIANATION_OBJ = <?php echo json_encode($wp_query_params);?>;
                </script>
                <div id="post--items" class="post--items">

                    <?php
                    if (have_posts()) :
                        /* Start the Loop */
                        while (have_posts()) :
                            the_post();
                            get_template_part('template-parts/content/preview');
                        endwhile;

                        $paginate = paginate_links(['type' => 'array',
                            'prev_text' => '<',
                            'next_text' => '>']);
                        if (!empty($paginate)) {
                            ?>
                            <nav class="pagination--nav pt--50">
                                <ul class="pagination clearfix">
                                    <?php
                                    foreach ($paginate as $link) {
                                        $class = '';
                                        if (strpos($link, 'current') !== false) {
                                            $class = 'active';
                                        }
                                        echo "<li class='$class'
                                        
                                         data-ajax-page='" .
                                            get_page_index_from_link($link) . "'>$link</li>";
                                    }
                                    ?>
                                </ul>
                            </nav>
                            <?php
                        }
                    endif;
                    ?>
                </div>
            </div>

            <?php include_once 'sidebar.php'; ?>

        </div>
    </div>
</section>
<?php get_footer(); ?>

