<?php

add_action('init', 'my_custom_init');

function my_custom_init()
{
    register_post_type('portfolio', array(
        'labels' => array(
            'name' => 'Portfolio',
            'menu_name' => 'Portfolio'

        ),
        'menu_icon' => 'dashicons-layout',
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields')
    ));

    register_taxonomy('technologies', array('portfolio', 'blog'), array(
        'label' => 'Technologies', // определяется параметром $labels->name
        'labels' => array(
            'name' => 'Technologies',
            'singular_name' => 'Technology',
            'search_items' => 'Search Technology',
            'all_items' => 'All Technology',
            'view_item ' => 'View Technology',
            'parent_item' => 'Parent Technology',
            'parent_item_colon' => 'Parent Technology:',
            'edit_item' => 'Edit Technology',
            'update_item' => 'Update Technology',
            'add_new_item' => 'Add New Technology',
            'new_item_name' => 'New Technology Name',
            'menu_name' => 'Technology',
        ),
        'description' => '', // описание таксономии
        'public' => true,
        'publicly_queryable' => null, // равен аргументу public
        'show_in_nav_menus' => true, // равен аргументу public
        'show_ui' => true, // равен аргументу public
        'show_in_menu' => true, // равен аргументу show_ui
        'show_tagcloud' => true, // равен аргументу show_ui
        'show_in_rest' => null, // добавить в REST API
        'rest_base' => null, // $taxonomy
        'hierarchical' => false,
        //'update_count_callback' => '_update_post_term_count',
        'rewrite' => true,
        //'query_var'             => $taxonomy, // название параметра запроса
        'capabilities' => array(),
        'meta_box_cb' => null, // html метабокса. callback: `post_categories_meta_box` или `post_tags_meta_box`. false — метабокс отключен.
        'show_admin_column' => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
        '_builtin' => false,
        'show_in_quick_edit' => null, // по умолчанию значение show_ui
    ));

    register_taxonomy('categories_portfolio', array('portfolio', 'blog'), array(
        'label' => '', // определяется параметром $labels->name
        'labels' => array(
            'name' => 'Categories',
            'singular_name' => 'Category',
            'search_items' => 'Search Category',
            'all_items' => 'All Categories',
            'view_item ' => 'View Category',
            'parent_item' => 'Parent Category',
            'parent_item_colon' => 'Parent Category:',
            'edit_item' => 'Edit Category',
            'update_item' => 'Update Category',
            'add_new_item' => 'Add New Category',
            'new_item_name' => 'New Category Name',
            'menu_name' => 'Category',
        ),
        'description' => '', // описание таксономии
        'public' => true,
        'publicly_queryable' => null, // равен аргументу public
        'show_in_nav_menus' => true, // равен аргументу public
        'show_ui' => true, // равен аргументу public
        'show_in_menu' => true, // равен аргументу show_ui
        'show_tagcloud' => true, // равен аргументу show_ui
        'show_in_rest' => null, // добавить в REST API
        'rest_base' => null, // $taxonomy
        'hierarchical' => true,
        //'update_count_callback' => '_update_post_term_count',
        'rewrite' => true,
        //'query_var'             => $taxonomy, // название параметра запроса
        'capabilities' => array(),
        'meta_box_cb' => null, // html метабокса. callback: `post_categories_meta_box` или `post_tags_meta_box`. false — метабокс отключен.
        'show_admin_column' => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
        '_builtin' => false,
        'show_in_quick_edit' => null, // по умолчанию значение show_ui
    ));

    register_post_type('blog', array(
        'labels' => array(
            'name' => 'Blog posts',
            'menu_name' => 'Blog posts'

        ),
        'menu_icon' => 'dashicons-book',
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'custom-fields')
    ));

}
