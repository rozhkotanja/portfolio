<?php
add_action('after_setup_theme', function () {
    add_theme_support('post-thumbnails');
});


add_action('wp_enqueue_scripts', 'my_scripts_add');
function my_scripts_add()
{
    wp_enqueue_style('style-name', get_template_directory_uri());
    wp_enqueue_style('style-favicon', get_template_directory_uri() . '/favicon.png');
    wp_enqueue_style('style-google-fonts', 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700%7CRoboto+Condensed:300,400,700');
    wp_enqueue_style('style-plugins_min_css', get_template_directory_uri() . '/css/plugins.min.css');
    wp_enqueue_style('style-main', get_template_directory_uri() . '/style.css');
    wp_enqueue_style('style-responsive', get_template_directory_uri() . '/css/responsive-style.css');
    wp_enqueue_style('style-color', get_template_directory_uri() . '/css/colors/color-7.css');
    wp_enqueue_style('style-custom', get_template_directory_uri() . '/css/custom.css');

    wp_enqueue_script('plugins', get_template_directory_uri() . '/js/plugins.min.js', array('jquery'), '1.1', true);
    wp_enqueue_script('main', get_template_directory_uri() . '/js/main.js', array('jquery'), '1.1', true);

    wp_localize_script( 'main', 'wp_ajax',
        array(
            'url' => admin_url('admin-ajax.php')
        )
    );

}



