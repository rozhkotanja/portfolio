<?php
add_action('wp_ajax_add_coment', 'add_coment_callback');
add_action('wp_ajax_nopriv_add_coment', 'add_coment_callback');
function add_coment_callback()
{
    $request = [];
    parse_str($_POST['data'], $request);

    try {
        if (!empty($request)) {
            $data = [
                'comment_post_ID' => intval($request['post_id']),
                'comment_author' => $request['name'],
                'comment_author_email' => $request['email'],
                'comment_content' => $request['message'],
                'comment_approved' => 0,
            ];

            $coment_id = wp_insert_comment(wp_slash($data));
            if ($coment_id) {
                wp_send_json_success(
                    [
                        'data' => $request,
                        'coment_id' => $coment_id,
                        'message' => 'Дякуємо, за ваш коментар, він буде опублікований найблищим часом, після перевірки адмінстратором'
                    ]);
            } else {
                wp_send_json_error(['data' => $request]);
            }
        } else {
            wp_send_json_error(['data' => $request]);
        }

    } catch
    (Exception $e) {
        wp_send_json_error(['data' => $request,
            'error' => $e->getMessage()]);
    }


    wp_send_json(['data' => $request]);
    wp_die();
}

add_action('wp_ajax_ajax_pagi', 'ajax_pagi_callback');
add_action('wp_ajax_ajax_pagi', 'ajax_pagi_callback');
function ajax_pagi_callback()
{
    $data = $_POST;
    $args = [
        "paged" => intval($data['data']['paged']),
        "post_type" => $data['data']['post_type'] ? $data['data']['post_type'] : 'any',
        "posts_per_page" => intval($data['data']['posts_per_page'])
    ];
    if($data['data']['taxonomy']){
        $args['tax_query'] =  [
            'taxonomy' => $data['data']['taxonomy'],
            'field'    => 'slug',
            'terms'    => $data['data']['term']
        ];
    }

    $query = new WP_Query($args);

    /* Start the Loop */
    while ($query->have_posts()) :
        $query->the_post();
           get_template_part('template-parts/content/preview');
    endwhile;
    $big = 999999999;
    $paginate = paginate_links(['type' => 'array',
        'prev_text' => '<',
        'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
        'format' => '?paged=%#%',
        'current' => max(1, $args['paged']),
        'total' => $query->max_num_pages,
        'next_text' => '>']);
    if (!empty($paginate)) {
        ?>
        <nav class="pagination--nav pt--50">
            <ul class="pagination clearfix">
                <?php
                foreach ($paginate as $link) {
                    $class = '';
                    if (strpos($link, 'current') !== false) {
                        $class = 'active';
                    }
                    echo "<li class='$class'
                                         data-ajax-page='" .
                        get_page_index_from_link($link) . "'>$link</li>";
                }
                ?>
            </ul>
        </nav>
        <?php
    }
    wp_die();
}

