<?php
add_action('after_setup_theme', 'function_after_setup_theme');

function function_after_setup_theme()
{

    register_nav_menus(array(
        'top' => 'Верхнее меню',    //Название месторасположения меню в шаблоне
        'bottom' => 'Нижнее меню'      //Название другого месторасположения меню в шаблоне
    ));

    add_theme_support('post-thumbnails');
    add_theme_support('menus');
    add_theme_support('widgets');
    add_theme_support('custom-logo');

    add_image_size('icon', 64, 64, true);
    add_image_size('full-hd-thumb', 1920, 1080, true);
    add_image_size('portfolio-img', 600, 485, true);
    add_image_size('my-team-image', 370, 450, true);
    add_image_size('blog-news-front-page-img', 600, 545, true);
    add_image_size('posts-image', 800, 379, true);

}


/*========= START acf option page ========*/

if (function_exists('acf_add_options_page')) {

    acf_add_options_page();

    acf_set_options_page_capability('manage_options');
    acf_set_options_page_title(__('Theme Options'));
}

/*========== END acf option page ========*/

class  Walker_Quickstart_Menu extends Walker
{

    // Tell Walker where to inherit it's parent and id values
    var $db_fields = array(
        'parent' => 'menu_item_parent',
        'id' => 'db_id'
    );

    /**
     * At the start of each element, output a <li> and <a> tag structure.
     *
     * Note: Menu objects include url and title properties, so we will use those.
     */
    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
    {
        $output .= sprintf(
            "\n<li><a href='%s'%s data-trigger='scroll'>%s</a></li>\n",
            $item->url,
            ($item->object_id === get_the_ID()) ? ' class="active"' : '',
            $item->title
        );
        // if ($this->has_children) {
        //     var_dump($item);
        //     $output .= sprintf(
        //         "\n<li class='dropdown'><a href='%s'%s class='dropdown-toggle' data-toggle='dropdown' aria-expanded='true'>
        //         %s <span class='caret'> </span>
        //         </a>
        //         <ul class='dropdown-menu'> %s </ul>
        //         </li>\n",
        //         $item->url,
        //         ($item->object_id === get_the_ID()) ? ' class="active"' : '',
        //         $item->title, 
        //         $item->title
        //     );                
        // } else {
        //     $output .= sprintf(
        //         "\n<li><a href='%s'%s data-trigger='scroll'>%s</a></li>\n",
        //         $item->url,
        //         ($item->object_id === get_the_ID()) ? ' class="active"' : '',
        //         $item->title
        //     );
        // }
    }
}

add_filter('navigation_markup_template', 'my_navigation_template', 10, 2);
function my_navigation_template($template, $class)
{
    /*
    Вид базового шаблона:
    <nav class="navigation %1$s" role="navigation">
        <h2 class="screen-reader-text">%2$s</h2>
        <div class="nav-links">%3$s</div>
    </nav>
    */

    return '
	<nav class="pagination--nav pt--50">
	    <ul class="pagination clearfix">
	    <li>
            <a href="#" aria-label="Previous">
                <i class="fa fa-angle-left" aria-hidden="true"></i>
            </a>
        </li>
        <li class="active"><span>1</span></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li>
            <a href="#" aria-label="Next">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </a>
        </li>
		    <div class="nav-links">%3$s</div>
		</ul>
	</nav>    
	';
}

add_action( 'save_post', 'save_all_meta' );
function save_all_meta( $post_id ) {
    $fields = get_fields($post_id);
    add_post_meta( $post_id, 'all_meta', $fields, true )
    or update_post_meta($post_id, 'all_meta', $fields );
}