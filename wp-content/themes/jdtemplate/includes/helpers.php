<?php

function splited_string($text, $count = 50, $endString = '...')
{

    $text_array = explode(' ', strip_tags($text));
    if ($count < count($text_array)) {
        $srt = '';
        for ($i = 0; $i < $count; $i++) {
            $srt .= ' '. $text_array[$i];
        }
        return trim($srt) . $endString;
    }
    return strip_tags($text . $endString);

}

function get_page_index_from_link($link){
    $re = '/page\/(\d)?\//';
    $ajax_re = '/paged=(\d)|(<\/a)/';
    preg_match($re, $link, $matches);
    preg_match($ajax_re, $link, $ajax_matches);

    if(!empty($matches[1][0]) ){
        $index = $matches[1][0];
        return $index;
    }else if(!empty($ajax_matches[1])){
        $index = $ajax_matches[1];
        return $index;
    }else{
        if(!empty($ajax_matches[2])){
            return 1;
        }
        return '';
    }
}

function get_archive_post_type() {
    return is_archive() ? get_queried_object()->name : false;
}