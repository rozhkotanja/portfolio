<!-- Footer Section Start -->
    
<?php $footer_data = get_field('footer', 'option'); ?>

        <footer class="footer--section pt--100 pb--100 text-center">
            <div class="container">
                <!-- Footer Social Start -->
                <div class="footer--social">

                <?php if (!empty($footer_data['socials'])) : ?>
                    <ul class="nav" data-scroll-reveal="group">
                        <?php foreach($footer_data['socials'] as $key=>$value): ?>
                            <li><a title="<?php echo $value['title']; ?>" href="<?php echo $value['link']; ?>">
                                <span class="<?php echo $value['icon']; ?>"></span>
                            </a></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif ?>
                </div>
                <!-- Footer Social End -->

                <!-- Copyright Text Start -->
                <p class="copyright--text fs--16 ff--primary">
                    <?php if ($footer_data['footer_text']) : ?>
                        <?php echo $footer_data['footer_text']; ?>
                    <?php endif ?>
                </p>
                <!-- Copyright Text End -->
            </div>
        </footer>
        <!-- Footer Section End -->
    </div>
    <!-- Wrapper End -->

    <!-- Back To Top Button Start -->
    <div id="backToTop">
        <a href="#" class="btn btn-default active"><i class="fa fa-long-arrow-up"></i></a>
    </div>
    <!-- Back To Top Button End -->    

    <?php wp_footer(); ?>

</body>
</html>
