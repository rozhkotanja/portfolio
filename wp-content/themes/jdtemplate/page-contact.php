<?php
get_header();
$contacts = get_fields();
?>

    <!-- Page Breadcrumb Start -->
    <div class="page--breadcrumb css-paralax pt--160 pb--100"
         style="background-image: url(<?php echo $contacts['image_contact']['url'];  ?>);"
         data-overlay="0.8">
        <div class="container">
            <ul class="breadcrumb ff--primary text-center">
                <li>
                    <a href="<?php echo home_url();   ?>" class="btn-link">
                        <?php echo _e('Головна'); ?>
                    </a>
                </li>
                <li class="active">
                    <span>
                        Contact
                    </span>
                </li>
            </ul>
        </div>
    </div>
    <!-- Page Breadcrumb End -->

    <!-- Contact Section Start -->
    <section id="contact" class="section pt--100 pb--40 bg-light">
        <div class="container">
            <!-- Section Title Start -->
            <div class="section--title pb--60 text-center">
                <h2 class="h2 text-uppercase">
                    <?php if (get_field('contact', 'option')['contact_title']): ?>
                        <?php echo get_field('contact', 'option')['contact_title'] ?>
                    <?php endif; ?>
                </h2>
            </div>


            <!-- Section Title End -->

            <div class="row AdjustRow pb--10">
                <?php if (!empty(get_field('contact', 'option')['contacts_info'])): ?>
                    <?php foreach (get_field('contact', 'option')['contacts_info'] as $key => $value) : ?>
                        <div class="col-md-4 col-xs-12 pb--40">
                            <!-- Contact Info Block Start -->
                            <div class="contact--info-block" data-scroll-reveal="bottom">
                                <div class="icon">
                                    <i class="<?php echo
                                    $value['contact_icon']; ?>">
                                    </i>
                                </div>
                                <div class="title text-uppercase">
                                    <h3 class="h4">
                                        <?php echo $value['contact_title']; ?>
                                    </h3>
                                </div>
                                <div class="desc">
                                    <p>
                                        <a href="<?php echo
                                        $value['contact_href']; ?>" class="btn-link">
                                            <?php echo $value['contact_description']; ?>
                                        </a>,
                                        <a href="<?php echo
                                        $value['contact_href_2']; ?>" class="btn-link">
                                            <?php echo $value['contact_description_2']; ?>
                                        </a>
                                    </p>
                                </div>
                            </div>
                            <!-- Contact Info Block End -->
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>

            <!-- Contact Form Start -->
            <div class="contact--form pb--60" data-form="ajax">
                <div class="title text-center text-uppercase pb--30">
                    <h3 class="h4">
                        <?php echo get_field('contact', 'option')['contact_form_title'];?>
                    </h3>
                </div>

                <?php echo get_field('contact', 'option')['contact_form'];?>

            </div>
            <!-- Contact Form End -->
        </div>
    </section>
    <!-- Contact Section End -->

<?php get_footer(); ?>