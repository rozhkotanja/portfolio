<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>
    <!-- 404 Section Start -->
    <section id="f0f" class="f0f--section" data-bg-img="img/404-img/bg.jpg" data-overlay="0.5">
            <div class="container">
                <!-- 404 Content Start -->
                <div class="f0f--content pt--160 pb--100 text-center text-white">
                    <div class="title text-uppercase">
                        <h2 class="h1 text-white">404</h2>
                        <!-- <p>Error</p> -->
                    </div>

                    <div class="sub-title pt--10 pb--20">
                        <h3 class="h2 text-white fw--400">
                            <?php echo __('Упс, ця сторінка не знайдена!', 'main'); ?>
                        </h3>
                    </div>

                    <div class="desc">
                        <p>          
                            <?php echo __('Сторінка, яку ви шукаєте, була переміщена, 
                            видалена, перейменована або ніколи не існувала. 
                            Повторіть спробу і ознайомтеся з нашим сайтом.', 'main'); ?>                 
                        </p>
                    </div>

                    <div class="search" data-form="validate">
                        <form action="#">
                            <input type="text" name="search" placeholder="Search..." class="form-control" required>

                            <button type="submit" class="btn btn-default active"><i class="fa fa-search"></i></button>
                        </form>
                    </div>

                    <div class="buttons">
                        <a href="<?php echo home_url();   ?>" class="btn btn-white">
                            <?php echo __('На головну', 'main'); ?>
                        </a>
                    </div>
                </div>
                <!-- 404 Content End -->
            </div>
        </section>
        <!-- 404 Section End -->


<?php
get_footer();
