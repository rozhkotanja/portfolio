<?php
get_header();
$img_thubnail = get_the_post_thumbnail_url(get_the_ID(), 'full');
?>

<!-- Page Breadcrumb Start -->
<div class="page--breadcrumb pt--160 pb--100" data-bg-parallax="<?php echo $img_thubnail; ?>" data-overlay="0.8">
    <div class="container">
        <ul class="breadcrumb ff--primary text-center">
            <li>
                <a href="<?php echo home_url(); ?>" class="btn-link">
                    <?php echo __('Головна'); ?>
                </a>
            </li>
            <li class="active">
                <span>
                    <?php echo __('Всі пости'); ?>
                </span>
            </li>
        </ul>
    </div>
</div>
<!-- Page Breadcrumb End -->

<?php

$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;

$args = array(
    'post_type' => array('blog', 'portfolio'),
    'posts_per_page' => get_option( 'posts_per_page' ),
    'paged'          => $paged,

);

$query = new WP_Query($args);



?>


<!-- Blog Section Start -->
<section id="blog" class="section pt--100 pb--40">
    <div class="container">
        <div class="row">
            <div class="col-md-8 pb--60">
                <!-- Post Items Start -->
                <div class="post--items">

                    <?php
                    if ($query->have_posts()) :
                        /* Start the Loop */
                        while ($query->have_posts()) :
                            $query->the_post();
                            get_template_part('template-parts/content/preview');
                        endwhile;
                        wp_reset_postdata();

                        $big = 999999999;


                        $paginate = paginate_links(['type' => 'array',
                            'prev_text' => '<',
                            'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                            'format'  => '?paged=%#%',
                            'current' => max( 1, get_query_var('paged') ),
                            'total'   => $query->max_num_pages,
                            'next_text' => '>']);
                        if (!empty($paginate)) {
                            ?>
                            <nav class="pagination--nav pt--50">
                                <ul class="pagination clearfix">
                                    <?php
                                    foreach ($paginate as $link) {
                                        $class = '';
                                        if (strpos($link, 'current') !== false) {
                                            $class = 'active';
                                        }
                                        echo "<li class='$class'>$link</li>";
                                    }
                                    ?>
                                </ul>
                            </nav>
                            <?php
                        }
                    endif;
                    ?>
                </div>
            </div>

            <?php include_once 'sidebar.php'; ?>

        </div>
    </div>
</section>

<?php get_footer(); ?>
