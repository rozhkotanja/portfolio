<?php ?>

<div class="col-md-4 pb--60">
    <!-- Widget Start -->
    <div class="widget">
        <!-- Search Widget Start -->
        <div class="search--widget" data-form="validate">
            <form action="<?php echo home_url( '/' ) ?>">
                <input type="text"
                       name="s"
                       placeholder="Search"
                       class="form-control"
                       value="<?php echo get_search_query() ?>"
                       required
                       id="s"
                >

                <button type="submit" class="btn-link">
                    <i class="fa fa-search"></i>
                </button>
                </button>
            </form>
        </div>

        <!-- Search Widget End -->
    </div>
    <!-- Widget End -->

    <!-- Widget Start -->
    <div class="widget" style="display: none; ">
        <div class="widget--title">
            <h2 class="h4">Newsletter</h2>
        </div>

        <!-- Newsletter Widget Start -->
        <div class="newsletter--widget bg-default" data-form="validate">
            <div class="desc">
                <p>Enter you email address below to subscribe to my newsletter.</p>
            </div>

            <form action="https://themelooks.us13.list-manage.com/subscribe/post?u=79f0b132ec25ee223bb41835f&id=f4e0e93d1d"
                  method="post" name="mc-embedded-subscribe-form" target="_blank">
                <input type="email" name="EMAIL" placeholder="Enter your email" class="form-control" autocomplete="off"
                       required>

                <button type="submit" class="btn btn-block btn-default active">Subscribe Now</button>
            </form>
        </div>
        <!-- Newsletter Widget End -->
    </div>
    <!-- Widget End -->

    <!-- Widget Start -->
    <div class="widget">
        <div class="widget--title">
            <h2 class="h4">
                <?php echo __('Categoriestbody');?>
            </h2>
        </div>

        <!-- Links Widget Start -->
        <div class="links--widget fs--16 ff--primary bg-default">
            <ul class="nav">
                <?php
                $terms = get_terms('categories_portfolio');
                if (!empty($terms)):
                    foreach ($terms as $term):
                        ?>
                        <li>
                            <?php echo "<a href='" . get_home_url() . "/categories_portfolio/$term->slug'> $term->name</a>"; ?>
                        </li>
                    <?php endforeach; endif; ?>
            </ul>
        </div>
        <!-- Links Widget End -->
    </div>
    <!-- Widget End -->

    <!-- Widget Start -->
    <div class="widget">
        <div class="widget--title">
            <h2 class="h4">
                <?php echo __('Popular Tags');?>
            </h2>
        </div>

        <!-- Links Widget Start -->
        <div class="tags--widget ff--primary bg-default">
            <ul class="nav">
                <?php
                $terms = get_terms('technologies');
                if (!empty($terms)):
                    foreach ($terms as $term):
                        ?>
                        <li>
                            <?php echo "<a href='" . get_home_url() . "/technologies/$term->slug'> $term->name</a>"; ?>
                        </li>
                    <?php endforeach; endif; ?>
            </ul>
        </div>
        <!-- Links Widget End -->
    </div>
    <!-- Widget End -->
</div>