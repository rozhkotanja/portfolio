
<?php get_header(); ?>

<?php 

    $data = get_fields();
//    var_dump($data);

?>


        <!-- About Section Start -->
        <section id="aboutme" class="section pt--100 pb--40">
            <div class="container">
                <!-- Section Title Start -->
                <div class="section--title pb--60 text-center">
                    <h2 class="h2 text-uppercase"> <?php echo get_field('about_me_text');?> </h2>
                </div>
                <!-- Section Title End -->

                <div class="row row--md-vc">
                    <div class="col-md-6 pb--60">
                        <!-- Image Block Start -->
                        <div class="img--block">
                            <img src="<?php echo get_field('about_me_main_image');?>" alt="">
                        </div>
                        <!-- Image Block End -->
                    </div>
                    
                    <div class="col-md-6 pb--60">
                        <!-- About Block Start -->
                        <div class="about--block">
                            <div class="title">
                                <h2 class="h4 fw--700 text-uppercase"><?php echo get_field('personal_details');?></h2>
                            </div>

                            <div class="details pt--30 text-dark"> 
                                <p><?php echo get_field('text_1');?></p>
                            </div>

                            <div class="details pt--30">
                                <p><?php echo get_field('text_2');?></p>
                            </div>

                            <div class="info pt--50">
                                <table class="ff--primary">
                                    <tbody>
                                        <?php foreach($data['about_me_contacts'] as $key=>$value): ?>
                                            <tr>
                                                <th>
                                                    <i class="<?php echo $value['about_me_icons'] ?>"></i>
                                                    <span><?php echo $value['about_me_title'] ?></span>
                                                </th>
                                                <td><?php echo $value['about_me_info'] ?></td>
                                            </tr>
                                        <?php endforeach; ?>      
                                    </tbody>
                                </table>
                            </div>

                            <div class="social text-dark pt--30">
                                <ul class="nav">
                                    <?php foreach($data['about_me_social'] as $key=>$value): ?>
                                        <li><a href="<?php echo $value['social_href']; ?>">
                                            <span class="<?php echo $value['social_icon']; ?>"></span>
                                        </a></li>
                                    <?php endforeach; ?>  
                                </ul>
                            </div>

                            <div class="action pt--60">
                                <a href="#" class="btn btn-default">Not Need</a>
                            </div>

                        </div>
                        <!-- About Block End -->
                    </div>
                </div>
            </div>
        </section>
        <!-- About Section End -->

        <!-- Services Section Start -->
        <section id="services" class="section pt--100 pb--40 bg-light">
            <div class="container">
                <!-- Section Title Start -->
                <div class="section--title pb--60 text-center">
                    <h2 class="h2 text-uppercase"><?php echo $data['services_main_title'] ?></h2>
                </div>
                <!-- Section Title End -->

                <div class="row AdjustRow" data-scroll-reveal="group">

                    <?php foreach($data['services_step'] as $key=>$value): ?>

                        <div class="col-md-3 col-xs-6 col-xxs-12 pb--60">
                            <!-- Service Block Start -->
                            <div class="service--block">
                                <div class="icon">
                                    <i class="<?php echo $value['services_icon'] ?>"></i>
                                </div>

                                <div class="title">
                                    <h3 class="h4"> <?php echo $value['services_title'] ?></h3>
                                </div>

                                <div class="desc">
                                    <p><?php echo $value['services_description'] ?></p>
                                </div>
                            </div>
                            <!-- Service Block End -->
                        </div>

                    <?php endforeach; ?>
                </div>
            </div>
        </section>
        <!-- Services Section End -->

        <!-- Portfolio Section Start -->
        <section id="portfolio" class="section pt--100 pb--40">
            <div class="container">
                <!-- Section Title Start -->
                <div class="section--title pb--60 text-center">
                    <h2 class="h2 text-uppercase">Portfolio</h2>
                </div>
                <!-- Section Title End -->

                <!-- Portfolio Filter Menu Start -->
                <div class="portfolio--filter-menu pb--50">
                    <ul class="nav">
                        <li data-target="*" class="active">All Works</li>
                        <li data-target="illustration">Illustration</li>
                        <li data-target="ui-design">UI Design</li>
                        <li data-target="branding">Branding Design</li>
                    </ul>
                </div>
                <!-- Portfolio Filter Menu End -->

                <!-- Portfolio Items Start -->
                <div class="portfolio--items row MasonryRow pb--30" data-trigger="gallery_popup">
                    <div class="col-md-3 col-xs-6 col-xxs-12 pb--30" data-cat="illustration ui-design">
                        <!-- Portfolio Item Start -->
                        <div class="portfolio--item">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/portfolio-img/01.jpg" alt="">

                            <a href="img/portfolio-img/01.jpg" class="caption">
                                <div class="vc--parent">
                                    <div class="vc--child">
                                        <h3 class="h6">Cover Design</h3>

                                        <p>Lorem ipsum dolor sit amet.</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!-- Portfolio Item End -->
                    </div>

                    <div class="col-md-3 col-xs-6 col-xxs-12 pb--30" data-cat="ui-design illustration">
                        <!-- Portfolio Item Start -->
                        <div class="portfolio--item">
                            <img src="<?php echo get_template_directory_uri();?>/img/portfolio-img/02.jpg" alt="">

                            <a href="img/portfolio-img/02.jpg" class="caption">
                                <div class="vc--parent">
                                    <div class="vc--child">
                                        <h3 class="h6">Business Card Design</h3>

                                        <p>Lorem ipsum dolor sit amet.</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!-- Portfolio Item End -->
                    </div>

                    <div class="col-md-3 col-xs-6 col-xxs-12 pb--30" data-cat="illustration branding">
                        <!-- Portfolio Item Start -->
                        <div class="portfolio--item">
                            <img src="<?php echo get_template_directory_uri();?>/img/portfolio-img/03.jpg" alt="">

                            <a href="img/portfolio-img/03.jpg" class="caption">
                                <div class="vc--parent">
                                    <div class="vc--child">
                                        <h3 class="h6">Candleholders Design</h3>

                                        <p>Lorem ipsum dolor sit amet.</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!-- Portfolio Item End -->
                    </div>

                    <div class="col-md-3 col-xs-6 col-xxs-12 pb--30" data-cat="branding illustration">
                        <!-- Portfolio Item Start -->
                        <div class="portfolio--item">
                            <img src="<?php echo get_template_directory_uri();?>/img/portfolio-img/04.jpg" alt="">

                            <a href="img/portfolio-img/04.jpg" class="caption">
                                <div class="vc--parent">
                                    <div class="vc--child">
                                        <h3 class="h6">Branding Design</h3>

                                        <p>Lorem ipsum dolor sit amet.</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!-- Portfolio Item End -->
                    </div>

                    <div class="col-md-3 col-xs-6 col-xxs-12 pb--30" data-cat="illustration ui-design">
                        <!-- Portfolio Item Start -->
                        <div class="portfolio--item">
                            <img src="<?php echo get_template_directory_uri();?>/img/portfolio-img/01.jpg" alt="">

                            <a href="img/portfolio-img/01.jpg" class="caption">
                                <div class="vc--parent">
                                    <div class="vc--child">
                                        <h3 class="h6">Cover Design</h3>

                                        <p>Lorem ipsum dolor sit amet.</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!-- Portfolio Item End -->
                    </div>

                    <div class="col-md-3 col-xs-6 col-xxs-12 pb--30" data-cat="ui-design illustration">
                        <!-- Portfolio Item Start -->
                        <div class="portfolio--item">
                            <img src="<?php echo get_template_directory_uri();?>/img/portfolio-img/02.jpg" alt="">

                            <a href="img/portfolio-img/02.jpg" class="caption">
                                <div class="vc--parent">
                                    <div class="vc--child">
                                        <h3 class="h6">Business Card Design</h3>

                                        <p>Lorem ipsum dolor sit amet.</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!-- Portfolio Item End -->
                    </div>

                    <div class="col-md-3 col-xs-6 col-xxs-12 pb--30" data-cat="illustration branding">
                        <!-- Portfolio Item Start -->
                        <div class="portfolio--item">
                            <img src="<?php echo get_template_directory_uri();?>/img/portfolio-img/03.jpg" alt="">

                            <a href="img/portfolio-img/03.jpg" class="caption">
                                <div class="vc--parent">
                                    <div class="vc--child">
                                        <h3 class="h6">Candleholders Design</h3>

                                        <p>Lorem ipsum dolor sit amet.</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!-- Portfolio Item End -->
                    </div>

                    <div class="col-md-3 col-xs-6 col-xxs-12 pb--30" data-cat="branding illustration">
                        <!-- Portfolio Item Start -->
                        <div class="portfolio--item">
                            <img src="<?php echo get_template_directory_uri();?>/img/portfolio-img/04.jpg" alt="">

                            <a href="img/portfolio-img/04.jpg" class="caption">
                                <div class="vc--parent">
                                    <div class="vc--child">
                                        <h3 class="h6">Branding Design</h3>

                                        <p>Lorem ipsum dolor sit amet.</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!-- Portfolio Item End -->
                    </div>
                </div>
                <!-- Portfolio Items End -->
            </div>
        </section>
        <!-- Portfolio Section End -->

        <!-- Call To Action Start -->
        <section class="section pt--100 pb--100" data-bg-parallax="img/call-to-action-img/bg.jpg" data-overlay="0.7">
            <div class="container">
                <!-- Call To Action Block Start -->
                <div class="cta--block text-center">
                    <div class="title">
                        <h2 class="h2 text-white">Have an exciting idea for a project?</h2>
                    </div>

                    <div class="content fs--20 pt--10 text-white">
                        <p>Lorem ipsum dolor sit amet, elit at debitis, consequuntur labore mollitia ea odit eum.</p>
                    </div>

                    <div class="action pt--20">
                        <a href="#" class="btn btn-white">Hire Me Now</a>
                    </div>
                </div>
                <!-- Call To Action Block End -->
            </div>
        </section>
        <!-- Call To Action End -->

        <!-- Resume Section Start -->
        <section id="resume" class="section pt--100 pb--100">
            <div class="container">
                <!-- Section Title Start -->
                <div class="section--title pb--60 text-center">
                    <h2 class="h2 text-uppercase">My Resume</h2>
                </div>
                <!-- Section Title End -->

                <!-- Timeline Block Start -->
                <div class="timeline--block">
                    <div class="title">
                        <h3 class="h5"><?php echo $data['education_title'] ?></h3>
                    </div>

                    <div class="sub-title">
                        <p class="fs--16"><?php echo $data['education_text'] ?></p>
                    </div>

                    <!-- Timeline Items Start -->
                    <ul class="timeline--items nav">
                        
                        <?php foreach($data['education_step'] as $key=>$value): ?>

                            <li>
                                <!-- Timeline Item Start -->
                                <div class="timeline--item">
                                    <div class="icon bg-default">
                                        <i class="fa fa-graduation-cap"></i>
                                    </div>

                                    <table class="table" data-scroll-reveal="bottom">
                                        <tr>
                                            <td>
                                                <p class="date bg-primary"><?php echo $value['education_date'] ?></p>

                                                <div class="bottom">
                                                    <h4 class="name h6 fw--700"><?php echo $value['education_academy'] ?></h4>

                                                    <p class="institute ff--primary"><?php echo $value['education_master'] ?></p>

                                                    <p class="location fs--12"><?php echo $value['education_city'] ?></p>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="content">
                                                    <p><?php echo $value['education_description'] ?></p>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!-- Timeline Item End -->
                            </li>

                        <?php endforeach; ?>

                    </ul>
                    <!-- Timeline Items End -->
                </div>
                <!-- Timeline Block End -->
            </div>
        </section>
        <!-- Resume Section End -->

        <!-- Experience Section Start -->
        <section class="section pt--100 pb--100 bg-light">
            <div class="container">
                <!-- Timeline Block Start -->
                <div class="timeline--block">
                    <div class="title">
                        <h3 class="h5"><?php echo $data['experience_title'] ?></h3>
                    </div>

                    <div class="sub-title">
                        <p class="fs--16"><?php echo $data['experience_text'] ?></p>
                    </div>

                    <!-- Timeline Items Start -->
                    <ul class="timeline--items nav">

                        <?php foreach ($data['experience_step'] as $key=>$value): ?>
                            
                            <li>
                                <!-- Timeline Item Start -->
                                <div class="timeline--item">
                                    <div class="icon bg-light">
                                        <i class="fa fa-briefcase"></i>
                                    </div>

                                    <table class="table" data-scroll-reveal="bottom">
                                        <tr>
                                            <td>
                                                <p class="date bg-primary"><?php echo $value['experience_date'] ?></p>

                                                <div class="bottom">
                                                    <h4 class="name h6 fw--700"><?php echo $value['experience_company_name'] ?></h4>

                                                    <p class="institute ff--primary"><?php echo $value['experience_position'] ?></p>

                                                    <p class="location fs--12"><?php echo $value['experience_city'] ?></p>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="content">
                                                    <p><?php echo $value['experience_description'] ?></p>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!-- Timeline Item End -->
                            </li>

                        <?php endforeach ?>

                    </ul>
                    <!-- Timeline Items End -->
                </div>
                <!-- Timeline Block End -->
            </div>
        </section>
        <!-- Experience Section End -->

        <!-- Skills Section Start -->
         
            


        <!-- Skills Section End -->

        <!-- Pricing Section Start -->
        <section id="pricing" class="section pt--100 pb--40 bg-light">
            <div class="container">
                <!-- Section Title Start -->
                <div class="section--title pb--60 text-center">
                    <h2 class="h2 text-uppercase">Pricing Plan</h2>
                </div>
                <!-- Section Title End -->

                <div class="row AdjustRow">
                    <div class="col-md-4 col-xs-12 pb--60">
                        <!-- Pricing Item Start -->
                        <div class="pricing--item">
                            <div class="pricing--header bg-primary">
                                <div class="plan">
                                    <h3 class="h3 text-primary">Starter</h3>
                                </div>

                                <div class="price ff--primary">
                                    <p>Form <span>$50</span></p>
                                </div>
                            </div>

                            <div class="pricing--features fs--16 ff--primary">
                                <ul class="nav">
                                    <li>Web Design Service</li>
                                    <li>Web Development Service</li>
                                    <li>Graphic Design Service</li>
                                    <li>SEO Service</li>
                                </ul>
                            </div>

                            <div class="pricing--action">
                                <a href="#" class="btn btn-primary">Order Now</a>
                            </div>
                        </div>
                        <!-- Pricing Item End -->
                    </div>

                    <div class="col-md-4 col-xs-12 pb--60">
                        <!-- Pricing Item Start -->
                        <div class="pricing--item">
                            <div class="pricing--header bg-primary">
                                <div class="plan">
                                    <h3 class="h3 text-primary">Starter</h3>
                                </div>

                                <div class="price ff--primary">
                                    <p>Form <span>$150</span></p>
                                </div>
                            </div>

                            <div class="pricing--features fs--16 ff--primary">
                                <ul class="nav">
                                    <li>Web Design Service</li>
                                    <li>Web Development Service</li>
                                    <li>Graphic Design Service</li>
                                    <li>SEO Service</li>
                                </ul>
                            </div>

                            <div class="pricing--action">
                                <a href="#" class="btn btn-primary">Order Now</a>
                            </div>
                        </div>
                        <!-- Pricing Item End -->
                    </div>

                    <div class="col-md-4 col-xs-12 pb--60">
                        <!-- Pricing Item Start -->
                        <div class="pricing--item">
                            <div class="pricing--header bg-primary">
                                <div class="plan">
                                    <h3 class="h3 text-primary">Starter</h3>
                                </div>

                                <div class="price ff--primary">
                                    <p>Form <span>$250</span></p>
                                </div>
                            </div>

                            <div class="pricing--features fs--16 ff--primary">
                                <ul class="nav">
                                    <li>Web Design Service</li>
                                    <li>Web Development Service</li>
                                    <li>Graphic Design Service</li>
                                    <li>SEO Service</li>
                                </ul>
                            </div>

                            <div class="pricing--action">
                                <a href="#" class="btn btn-primary">Order Now</a>
                            </div>
                        </div>
                        <!-- Pricing Item End -->
                    </div>
                </div>
            </div>
        </section>
        <!-- Pricing Section End -->

        <!-- Team Section Start -->
        <section id="team" class="section pt--100 pb--40">
            <div class="container">
                <!-- Section Title Start -->
                <div class="section--title pb--60 text-center">
                    <h2 class="h2 text-uppercase">Meet My Team</h2>
                </div>
                <!-- Section Title End -->

                <div class="row AdjustRow">
                    <div class="col-md-4 col-xs-12 pb--60">
                        <!-- Team Member Item Start -->
                        <div class="team--member" data-scroll-reveal="bottom">
                            <div class="img">
                                <img src="<?php echo get_template_directory_uri();?>/img/team-img/member-01.jpg" alt="">
                            </div>

                            <div class="caption">
                                <div class="vc--parent">
                                    <div class="vc--child">
                                        <div class="name">
                                            <h3 class="h3 fw--400">Mike Jones</h3>
                                        </div>

                                        <div class="company ff--primary">
                                            <p>ThemeLooks</p>
                                        </div>

                                        <div class="social">
                                            <ul class="nav">
                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Team Member Item End -->
                    </div>

                    <div class="col-md-4 col-xs-12 pb--60">
                        <!-- Team Member Item Start -->
                        <div class="team--member" data-scroll-reveal="bottom">
                            <div class="img">
                                <img src="<?php echo get_template_directory_uri();?>/img/team-img/member-02.jpg" alt="">
                            </div>

                            <div class="caption">
                                <div class="vc--parent">
                                    <div class="vc--child">
                                        <div class="name">
                                            <h3 class="h3 fw--400">Jack Roberts</h3>
                                        </div>

                                        <div class="company ff--primary">
                                            <p>ThemeLooks</p>
                                        </div>

                                        <div class="social">
                                            <ul class="nav">
                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Team Member Item End -->
                    </div>

                    <div class="col-md-4 col-xs-12 pb--60">
                        <!-- Team Member Item Start -->
                        <div class="team--member" data-scroll-reveal="bottom">
                            <div class="img">
                                <img src="<?php echo get_template_directory_uri();?>/img/team-img/member-03.jpg" alt="">
                            </div>

                            <div class="caption">
                                <div class="vc--parent">
                                    <div class="vc--child">
                                        <div class="name">
                                            <h3 class="h3 fw--400">Dennis Murray</h3>
                                        </div>

                                        <div class="company ff--primary">
                                            <p>ThemeLooks</p>
                                        </div>

                                        <div class="social">
                                            <ul class="nav">
                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Team Member Item End -->
                    </div>
                </div>
            </div>
        </section>
        <!-- Team Section End -->

        <!-- Blog Section Start -->
        <section id="blog" class="section pt--100 pb--40 bg-light">
            <div class="container">
                <!-- Section Title Start -->
                <div class="section--title pb--60 text-center">
                    <h2 class="h2 text-uppercase">Blog &amp; News</h2>
                </div>
                <!-- Section Title End -->

                <div class="row AdjustRow">
                    <div class="col-md-4 col-xs-12 pb--60">
                        <!-- Post Item Start -->
                        <div class="post--item">
                            <!-- Post Image Start -->
                            <div class="post--img">
                                <a href="blog-details.html"><img src="<?php echo get_template_directory_uri();?>/img/blog-img/post-item-01.jpg" alt=""></a>

                                <a href="#" class="date">25 Jan 2017</a>
                            </div>
                            <!-- Post Image End -->

                            <!-- Post Title Start -->
                            <div class="post--title text-uppercase">
                                <h3 class="h3 fs--22">
                                    <a href="blog-details.html" class="btn-link">Eiusmod tempor incididunt ut labore et dolor menna aliqua</a>
                                </h3>
                            </div>
                            <!-- Post Title End -->

                            <!-- Post Excerpt Start -->
                            <div class="post--excerpt">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi dignissimos molestiae voluptates ullam dolor mollitia quos commodi...</p>
                            </div>
                            <!-- Post Excerpt End -->

                            <!-- Post Action Start -->
                            <div class="post--action text-uppercase">
                                <a href="blog-details.html" class="btn-link">Read More<i class="fa fa-long-arrow-right"></i></a>
                            </div>
                            <!-- Post Action End -->
                        </div>
                        <!-- Post Item End -->
                    </div>

                    <div class="col-md-4 col-xs-12 pb--60">
                        <!-- Post Item Start -->
                        <div class="post--item">
                            <!-- Post Image Start -->
                            <div class="post--img">
                                <a href="blog-details.html"><img src="<?php echo get_template_directory_uri();?>/img/blog-img/post-item-02.jpg" alt=""></a>

                                <a href="#" class="date">25 Jan 2017</a>
                            </div>
                            <!-- Post Image End -->

                            <!-- Post Title Start -->
                            <div class="post--title text-uppercase">
                                <h3 class="h3 fs--22">
                                    <a href="blog-details.html" class="btn-link">Eiusmod tempor incididunt ut labore et dolor menna aliqua</a>
                                </h3>
                            </div>
                            <!-- Post Title End -->

                            <!-- Post Excerpt Start -->
                            <div class="post--excerpt">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi dignissimos molestiae voluptates ullam dolor mollitia quos commodi...</p>
                            </div>
                            <!-- Post Excerpt End -->

                            <!-- Post Action Start -->
                            <div class="post--action text-uppercase">
                                <a href="blog-details.html" class="btn-link">Read More<i class="fa fa-long-arrow-right"></i></a>
                            </div>
                            <!-- Post Action End -->
                        </div>
                        <!-- Post Item End -->
                    </div>

                    <div class="col-md-4 col-xs-12 pb--60">
                        <!-- Post Item Start -->
                        <div class="post--item">
                            <!-- Post Image Start -->
                            <div class="post--img">
                                <a href="blog-details.html"><img src="<?php echo get_template_directory_uri();?>/img/blog-img/post-item-03.jpg" alt=""></a>

                                <a href="#" class="date">25 Jan 2017</a>
                            </div>
                            <!-- Post Image End -->

                            <!-- Post Title Start -->
                            <div class="post--title text-uppercase">
                                <h3 class="h3 fs--22">
                                    <a href="blog-details.html" class="btn-link">Eiusmod tempor incididunt ut labore et dolor menna aliqua</a>
                                </h3>
                            </div>
                            <!-- Post Title End -->

                            <!-- Post Excerpt Start -->
                            <div class="post--excerpt">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi dignissimos molestiae voluptates ullam dolor mollitia quos commodi...</p>
                            </div>
                            <!-- Post Excerpt End -->

                            <!-- Post Action Start -->
                            <div class="post--action text-uppercase">
                                <a href="blog-details.html" class="btn-link">Read More<i class="fa fa-long-arrow-right"></i></a>
                            </div>
                            <!-- Post Action End -->
                        </div>
                        <!-- Post Item End -->
                    </div>
                </div>

                <!-- Section Footer Start -->
                <div class="section--footer text-center pb--60">
                    <a href="blog.html" class="btn btn-primary">View All Posts</a>
                </div>
                <!-- Section Footer End -->
            </div>
        </section>
        <!-- Blog Section End -->

        <!-- Clients Section Start -->
        <section id="clients" class="section pt--80 pb--20">
            <div class="container">
                <div class="row" data-scroll-reveal="group">
                    <div class="col-md-3 col-xs-6 col-xxs-12 pb--60">
                        <!-- Client Item Start -->
                        <div class="client--item">
                            <img src="<?php echo get_template_directory_uri();?>/img/brands-img/01.png" alt="" class="center-block">
                        </div>
                        <!-- Client Item End -->
                    </div>

                    <div class="col-md-3 col-xs-6 col-xxs-12 pb--60">
                        <!-- Client Item Start -->
                        <div class="client--item">
                            <img src="<?php echo get_template_directory_uri();?>/img/brands-img/02.png" alt="" class="center-block">
                        </div>
                        <!-- Client Item End -->
                    </div>

                    <div class="col-md-3 col-xs-6 col-xxs-12 pb--60">
                        <!-- Client Item Start -->
                        <div class="client--item">
                            <img src="<?php echo get_template_directory_uri();?>/img/brands-img/03.png" alt="" class="center-block">
                        </div>
                        <!-- Client Item End -->
                    </div>

                    <div class="col-md-3 col-xs-6 col-xxs-12 pb--60">
                        <!-- Client Item Start -->
                        <div class="client--item">
                            <img src="<?php echo get_template_directory_uri();?>/img/brands-img/04.png" alt="" class="center-block">
                        </div>
                        <!-- Client Item End -->
                    </div>
                </div>
            </div>
        </section>
        <!-- Clients Section End -->

        <!-- Testimonial Section Start -->
        <section id="testimonial" class="section pt--90 pb--90" data-bg-parallax="img/testimonial-img/bg.jpg" data-overlay="0.5">
            <div class="container">
                <!-- Testimonial Slider Start -->
                <div class="testimonial--slider owl-carousel" data-owl-dots="true">

                     <?php foreach($data['reviews_info'] as $key=>$value): ?>

                        <!-- Testimonial Item Start -->
                        <div class="testimonial--item text-white">
                            <div class="icon">
                                <i class="fa fa-comments"></i>
                            </div>

                            <blockquote>
                                <p><?php echo $value['reviews_text'] ?></p>

                                <footer><?php echo $value['reviews_signature'] ?></footer>
                            </blockquote>
                        </div>
                        <!-- Testimonial Item End -->      

                    <?php endforeach; ?>
            </div>
        </section>
        <!-- Testimonial Section End -->

        <!-- Contact Section Start -->
        <section id="contact" class="section pt--100 pb--40 bg-light">
            <div class="container">
                <!-- Section Title Start -->
                <div class="section--title pb--60 text-center">
                    <h2 class="h2 text-uppercase"><?php echo $data['contact_title'] ?></h2>
                </div>
                <!-- Section Title End -->

                <div class="row AdjustRow pb--10">

                    <?php foreach($data['contacts_info'] as $key=>$value): ?>

                        <div class="col-md-4 col-xs-12 pb--40">
                            <!-- Contact Info Block Start -->
                            <div class="contact--info-block" data-scroll-reveal="bottom">
                                <div class="icon">
                                    <i class="<?php echo $value['contact_icon']; ?>"></i>
                                </div>

                                <div class="title text-uppercase">
                                    <h3 class="h4"><?php echo $value['contact_title']; ?></h3>
                                </div>

                                <div class="desc">
                                    <p>
                                        <a href="<?php echo $value['contact_href']; ?>" class="btn-link">
                                            <?php echo $value['contact_description']; ?> 
                                        </a>, 
                                        <a href="<?php echo $value['contact_href_2']; ?>" class="btn-link">
                                            <?php echo $value['contact_description_2']; ?>
                                        </a>
                                    </p> 
                                </div>
                            </div>
                            <!-- Contact Info Block End -->
                        </div>

                    <?php endforeach; ?>
                </div>

                <!-- Contact Form Start -->
                <div class="contact--form pb--60" data-form="ajax">
                    <div class="title text-center text-uppercase pb--30">
                        <h3 class="h4">You can drop me a line here</h3>
                    </div>

                    <form action="forms/contact-form.php">
                        <div class="status"></div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" name="name" placeholder="Name *" class="form-control" required>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="email" name="email" placeholder="E-mail *" class="form-control" required>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" name="subject" placeholder="Subject *" class="form-control" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea name="message" placeholder="Message *" class="form-control" required></textarea>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <button type="submit" class="btn btn-block btn-primary">Send Message</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- Contact Form End -->
            </div>
        </section>
        <!-- Contact Section End -->

        
<?php get_footer(); ?>