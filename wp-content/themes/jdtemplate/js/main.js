/*

[Main Script]

Project     : JD - Responsive Personal vCard / CV / Resume Template
Version     : 1.0
Author      : ThemeLooks
Author URI  : https://themeforest.net/user/themelooks

*/

var validation = {
    isEmailAddress: function (str) {
        var pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return pattern.test(str);  // returns a boolean
    },
    isNotEmpty: function (str) {
        var pattern = /\S+/;
        return pattern.test(str);  // returns a boolean
    },
    isNumber: function (str) {
        var pattern = /^\d+$/;
        return pattern.test(str);  // returns a boolean
    },
    isSame: function (str1, str2) {
        return str1 === str2;
    }
};

(function ($) {
    "use strict";

    /* ------------------------------------------------------------------------- *
     * COMMON VARIABLES
     * ------------------------------------------------------------------------- */
    var $wn = $(window),
        $document = $(document),
        $body = $('body'),
        isRTL = $('html').attr('dir') === 'rtl' ? true : false;

    /* ------------------------------------------------------------------------- *
     * CHECK DATA
     * ------------------------------------------------------------------------- */
    var checkData = function (data, value) {
        return typeof data === 'undefined' ? value : data;
    };

    $(function () {
        /* ------------------------------------------------------------------------- *
         * BACKGROUND IMAGE
         * ------------------------------------------------------------------------- */
        var $bgImg = $('[data-bg-img]');

        $bgImg.css('background-image', function () {
            return 'url("' + $(this).data('bg-img') + '")';
        }).addClass('bg--img').removeAttr('data-bg-img').attr('data-rjs', 2);

        /* ------------------------------------------------------------------------- *
         * BACKGROUND PARALLAX
         * ------------------------------------------------------------------------- */
        var $parallaxBgImg = $('[data-bg-parallax]');

        $parallaxBgImg.each(function () {
            var $t = $(this);

            $t.parallax({imageSrc: $t.data('bg-parallax')}).addClass('bg--img');
        });

        /* ------------------------------------------------------------------------- *
         * RETINAJS
         * ------------------------------------------------------------------------- */
        $('img').attr('data-rjs', 2);

        retinajs();

        /* ------------------------------------------------------------------------- *
         * POPUP
         * ------------------------------------------------------------------------- */
        var $galleryPopup = $('[data-trigger="gallery_popup"]');

        if ($galleryPopup.length) {
            $galleryPopup.magnificPopup({
                delegate: 'a',
                type: 'image',
                mainClass: 'mfp-no-margins mfp-with-zoom',
                gallery: {
                    enabled: true
                }
            });
        }

        /* ------------------------------------------------------------------------- *
         * PROGRESS BAR
         * ------------------------------------------------------------------------- */
        var $progressBar = $('[data-trigger="progress_bar"]');

        $progressBar.each(function () {
            var $t = $(this),
                width = $t[0].style.width;

            $t.css('width', 0);

            $t.waypoint(function () {
                $t.css('width', width);
            }, {
                triggerOnce: true,
                offset: 'bottom-in-view'
            });
        });



        /* ------------------------------------------------------------------------- *
         * SCROLL
         * ------------------------------------------------------------------------- */
        var $scroll = $('[data-trigger="scroll"]');

        $scroll.on('click', function (e) {
            e.preventDefault();
            var href = $(this).attr('href');
            e.$target = href;
            var elementToScroll = $(e.$target);
            console.log(elementToScroll);
            if (elementToScroll.length && window.location.pathname == "/") {
                elementToScroll.animatescroll({
                    padding: 80,
                    easing: 'easeInOutExpo',
                    scrollSpeed: 2000
                });
            } else {
                window.location.href = window.location.origin + href;
            }


        });

        /* -------------------------------------------------------------------------*
         * COUNTDOWN
         * -------------------------------------------------------------------------*/
        var $countDown = $('[data-countdown]');

        $countDown.each(function () {
            var $t = $(this);

            $t.countdown($t.data('countdown'), function (e) {
                $(this).html('<ul>' + e.strftime('<li><strong>%D</strong><span>DAYS</span></li><li><strong>%H</strong><span>HOURS</span></li><li><strong>%M</strong><span>MINUTES</span></li><li><strong>%S</strong><span>SECONDS</span></li>') + '</ul>');
            });
        });

        /* ------------------------------------------------------------------------- *
         * OWL CAROUSEL
         * ------------------------------------------------------------------------- */
        var $owlCarousel = $('.owl-carousel');

        $owlCarousel.each(function () {
            var $t = $(this);

            $t.owlCarousel({
                rtl: isRTL,
                items: checkData($t.data('owl-items'), 1),
                margin: checkData($t.data('owl-margin'), 0),
                loop: checkData($t.data('owl-loop'), true),
                smartSpeed: 2500,
                autoplay: checkData($t.data('owl-autoplay'), true),
                autoplayTimeout: checkData($t.data('owl-speed'), 8000),
                center: checkData($t.data('owl-center'), false),
                animateOut: checkData($t.data('owl-animate'), false),
                nav: checkData($t.data('owl-nav'), false),
                navText: ['<i class="fa fa-caret-left"></i>', '<i class="fa fa-caret-right"></i>'],
                dots: checkData($t.data('owl-dots'), false),
                responsive: checkData($t.data('owl-responsive'), {})
            }).addClass('owl-dots--' + checkData($t.data('owl-dots-style'), '1'));
        });

        /* ------------------------------------------------------------------------- *
         * HEADER SECTION
         * ------------------------------------------------------------------------- */
        var $headerNav = $('#headerNav');

        $headerNav.on('click', 'a[data-trigger="scroll"]', function () {
            $headerNav.collapse('hide');
        });

        /* ------------------------------------------------------------------------- *
         * BANNER SECTION
         * ------------------------------------------------------------------------- */
        var $banner = $('.banner--section'),
            bannerMinHeight = function () {
                $banner.css('min-height', function () {
                    var $t = $(this);

                    return $t.find('.banner--content').outerHeight();
                });
            };

        bannerMinHeight(0);
        $wn.on('resize', bannerMinHeight);

        /* ------------------------------------------------------------------------- *
         * 404 SECTION
         * ------------------------------------------------------------------------- */
        var $f0f = $('.f0f--section'),
            f0fMinHeight = function () {
                $f0f.css('min-height', function () {
                    var $t = $(this);

                    return $t.find('.f0f--content').outerHeight();
                });
            };

        f0fMinHeight(0);
        $wn.on('resize', f0fMinHeight);

        /* ------------------------------------------------------------------------- *
         * Coming Soon SECTION
         * ------------------------------------------------------------------------- */
        var $comingSoon = $('.coming-soon--section'),
            comingSoonMinHeight = function () {
                $comingSoon.css('min-height', function () {
                    var $t = $(this);

                    return $t.find('.cs--content').outerHeight();
                });
            };

        comingSoonMinHeight(0);
        $wn.on('resize', comingSoonMinHeight);

        /* ------------------------------------------------------------------------- *
         * BACK TO TOP BUTTON
         * ------------------------------------------------------------------------- */
        var $backToTop = $('#backToTop');

        $backToTop.on('click', 'a', function (e) {
            e.preventDefault();

            $('html, body').animate({
                scrollTop: 0
            }, 1200);
        });
    });

    $wn.on('load', function () {
        /* ------------------------------------------------------------------------- *
         * BODY SCROLLING
         * ------------------------------------------------------------------------- */
        var isBodyScrolling = function () {
            if ($wn.scrollTop() > 1) {
                $body.addClass('isScrolling');
            } else {
                $body.removeClass('isScrolling');
            }
        };

        isBodyScrolling();
        $wn.on('scroll', isBodyScrolling);

        /* ------------------------------------------------------------------------- *
         * ADJUST ROW
         * ------------------------------------------------------------------------- */
        var $adjustRow = $('.AdjustRow');

        if ($adjustRow.length) {
            $adjustRow.isotope({
                originLeft: isRTL ? false : true,
                layoutMode: 'fitRows'
            });

            $adjustRow.isotope('on', 'arrangeComplete', function () {
                // Recalculate parallax dimensions
                $wn.trigger('resize.px.parallax');
            });
        }

        /* ------------------------------------------------------------------------- *
         * Masonry ROW
         * ------------------------------------------------------------------------- */
        var $masonryRow = $('.MasonryRow');

        if ($masonryRow.length) {
            $masonryRow.isotope({
                originLeft: isRTL ? false : true
            });

            $masonryRow.isotope('on', 'arrangeComplete', function () {
                // Recalculate parallax dimensions
                $wn.trigger('resize.px.parallax');
            });
        }

        /* ------------------------------------------------------------------------- *
         * PORTFOLIO ITEMS
         * ------------------------------------------------------------------------- */
        var $portfolioFilterMenu = $('.portfolio--filter-menu');

        $portfolioFilterMenu.on('click', 'li', function (e) {
            e.preventDefault();

            var $t = $(this),
                f = $t.data('target'),
                s = (f !== '*') ? '[data-cat~="' + f + '"]' : f,
                $portfolioItems = $t.parents('.portfolio--filter-menu').siblings('.portfolio--items');

            if ($portfolioItems.length) {
                $portfolioItems.isotope({
                    filter: s
                });
            }

            $t.addClass('active').siblings().removeClass('active');
        });

        /* ------------------------------------------------------------------------- *
         * PRELOADER
         * ------------------------------------------------------------------------- */
        var $preloader = $('#preloader');

        if ($preloader.length) {
            $preloader.fadeOut('slow');
        }

        /* ------------------------------------------------------------------------- *
         * SCROLLING ANIMATIONS
         * ------------------------------------------------------------------------- */
        var $scrollRevealGroup = $('[data-scroll-reveal="group"]'),
            scrollReveal = '';

        if (typeof ScrollReveal === "function") {
            scrollReveal = ScrollReveal();

            scrollReveal
                .reveal('[data-scroll-reveal="left"]', {origin: 'left', mobile: false, duration: 800})
                .reveal('[data-scroll-reveal="right"]', {origin: 'right', mobile: false, duration: 800})
                .reveal('[data-scroll-reveal="bottom"]', {duration: 800});

            $scrollRevealGroup.each(function () {
                scrollReveal.reveal($(this).children(), {duration: 800}, 150);
            });
        }
    });


    /* ------------------------------------------------------------------------- *
    * COMMENT FORM WITH VALIDATION
    * ------------------------------------------------------------------------- */
    $('#custom_comment_form').on('submit', function (event) {
        event.preventDefault();
        var form = this;


        $.each($(this).find('input, select, textarea'),
            function (key, element) {
                var $value = element.value;

                if($(element).is('.required')){
                    if(!validation.isNotEmpty($value)){
                        $(element).addClass('invalid');
                    }
                }
                if($(element).is('.email')){
                    if(!validation.isEmailAddress($value)){
                        $(element).addClass('invalid');
                    }
                }
                if($(element).is('.number')){
                    if(!validation.isNumber($value)){
                        $(element).addClass('invalid');
                    }
                }
            });
        var isInvalid = this.querySelector('.invalid');
        if(isInvalid){
            isInvalid.focus();
        }else{
            // go to ajax

            var formData = $(this).serialize();
            $.ajax({
                url: wp_ajax['url'],
                method: 'post',
                data: {
                    'action': 'add_coment',
                    data: formData
                },
                success: function (response) {
                    $('#form-message').css({'display': 'block'});
                    $.each($(form).find('input, select, textarea, button'), function(){
                        $(this).val('');
                        $(this).prop('disabled', true);
                    })
                    $('#form-message').html(response['data']['message']);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }

    });
    $(document).on('change', '.invalid', function () {
        $(this).removeClass('invalid');
    });

    // ajax pagination
    $(document).on('click', '[data-ajax-page]', function (e) {
        e.preventDefault();
        var paged = $(this).attr('data-ajax-page');
        if(paged){
            PAGIANATION_OBJ['paged']  = paged;
            $.ajax({
                url: wp_ajax['url'],
                method: 'post',
                data: {
                    'action': 'ajax_pagi',
                    data: PAGIANATION_OBJ
                },
                success: function (response) {
                    $("#post--items").html(response);
                    var title = PAGIANATION_OBJ['post_type'] || PAGIANATION_OBJ['taxonomy'];
                    var page = +PAGIANATION_OBJ['paged'];
                    ShowVisible();
                    $('html,body').animate({
                        scrollTop: $('#post--items').offset().top
                    }, 500);
                    window.history.pushState("Page "+ page , title, '/'+title+ '/page/'+ page);

                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    });

})(jQuery);

//LAZY LOADING IMAGE
document.addEventListener('DOMContentLoaded', ShowVisible);
window.addEventListener('scroll', ShowVisible);

function ShowVisible() {
    var imagesToLoad = document.querySelectorAll('[lazy-img-src]');
    imagesToLoad.forEach(function (element) {

        if (CanLoad(element)) {
            LoadImage(element);
        }

    });
}


// check if element in viewport
function CanLoad(elem) {
    var coords = elem.getBoundingClientRect();

    var windowHeight = document.documentElement.clientHeight;

    var topVisible = coords.top > 0 && coords.top < windowHeight;
    var bottomVisible = coords.bottom < windowHeight && coords.bottom > 0;

    return topVisible || bottomVisible;
};

// load image
function LoadImage(element) {
    var elementSrc = element.getAttribute('lazy-img-src');
    element.removeAttribute('lazy-img-src');
    element.setAttribute('src', elementSrc);
}

