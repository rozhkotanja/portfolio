<?php
get_header();
//$data = get_fields();
$img_thubnail = get_the_post_thumbnail_url(get_the_ID(), 'full');
the_post();
?>

<!-- Page Breadcrumb Start -->
<div class="page--breadcrumb pt--160 pb--100" data-bg-parallax="<?php echo $img_thubnail; ?>" data-overlay="0.8">
    <div class="container">
        <ul class="breadcrumb ff--primary text-center">
            <li>
                <a href="<?php echo home_url(); ?>" class="btn-link">
                    <?php echo _e('Головна'); ?>
                </a>
            </li>
            <li class="active">
                <span>
                    Blog Details
                </span>
            </li>
        </ul>
    </div>
</div>
<!-- Page Breadcrumb End -->

<!-- Blog Section Start -->
<section id="blog" class="section pt--100 pb--40">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <!-- Post Single Start -->
                <div class="post--single pb--60">
                    <div class="post--header clearfix">
                        <!-- Post Date Start -->
                        <div class="post--date ff--primary bg-primary">
                            <p>
                                <?php echo get_the_date('j F Y'); ?>
                            </p>
                        </div>
                        <!-- Post Date End -->

                        <!-- Post Title Start -->
                        <div class="post--title text-uppercase ov--h">
                            <h3 class="h3 fw--400">
                                <?php echo get_the_title(); ?>
                            </h3>
                        </div>
                        <!-- Post Title End -->
                    </div>

                    <!-- Post Image Start -->
                    <div class="post--img">
                        <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'posts-image'); ?>" alt="">
                    </div>
                    <!-- Post Image End -->

                    <!-- Post Meta Start -->
                    <div class="post--meta ff--primary pb--20">
                        <ul class="nav">
                            <li style="display:none">
                                <i class="fa fa-user"></i>
                                <a href="#">John Doe</a>
                            </li>
                            <li style="display:none">
                                <i class="fa fa-eye"></i>
                                <span>12</span>
                            </li>
                            <li style="display:none">
                                <i class="fa fa-comment"></i>
                                <a href="#">12</a>
                            </li>
                            <li>
                                <i class="fa fa-folder-open"></i>
                                <?php
                                $terms = get_the_terms(get_the_ID(), 'categories_portfolio');
                                if (!empty($terms)) {
                                    foreach ($terms as $term) {
                                        echo "<a href='" . get_home_url() . "/categories_portfolio/$term->slug'> $term->name</a>";
                                    }
                                }
                                ?>
                            </li>
                            <li>
                                <i class="fa fa-tags"></i>
                                <?php
                                $terms = get_the_terms(get_the_ID(), 'technologies');
                                if (!empty($terms)) {
                                    foreach ($terms as $term) {
                                        echo "<a href='" . get_home_url() . "/categories_portfolio/$term->slug'> $term->name</a>";
                                    }
                                }
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- Post Meta End -->

                    <!-- Post Content Start -->
                    <div class="post--content clearfix">

                        <?php echo get_the_content(); ?>

                    </div>
                    <!-- Post Content End -->

                    <!-- Post Footer Start -->
                    <div class="post--footer clearfix">
                        <!-- Post Share Start -->

                        <?php include_once 'template-parts/blog/share.php'; ?>

                        <!-- Post Share End -->

                    </div>
                    <!-- Post Footer End -->
                </div>
                <!-- Post Single End -->

                <!-- Comment List Start -->

                <?php include_once 'template-parts/blog/comment-list.php'; ?>

                <!-- Comment List End -->

                <!-- Comment Block Start -->

                <?php include_once 'template-parts/blog/comment-block.php'; ?>

                <!-- Comment Block End -->
            </div>

            <?php include_once 'sidebar.php'; ?>

        </div>
    </div>
</section>
<!-- Blog Section End -->


<?php get_footer(); ?>
