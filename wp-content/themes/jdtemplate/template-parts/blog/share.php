<!-- Post Share Start -->
<div class="post--share float--left">
    <span><i class="fa fa-share-alt"></i></span>

    <ul class="nav">
        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
    </ul>
</div>
<!-- Post Share End -->