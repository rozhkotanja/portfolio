<!-- Post Item Start -->
<div class="post--item">
    <!-- Post Image Start -->
    <div class="post--img">
        <a href="blog-details.html"><img src="<?php echo get_template_directory_uri(); ?>/img/blog-img/post-item-01.jpg" alt=""></a>

        <a href="#" class="date">25 Jan 2017</a>
    </div>
    <!-- Post Image End -->

    <!-- Post Title Start -->
    <div class="post--title text-uppercase">
        <h3 class="h3 fs--22">
            <a href="blog-details.html" class="btn-link">Eiusmod tempor incididunt ut labore et dolor menna aliqua</a>
        </h3>
    </div>
    <!-- Post Title End -->

    <!-- Post Excerpt Start -->
    <div class="post--excerpt">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi dignissimos molestiae voluptates ullam dolor mollitia quos commodi...</p>
    </div>
    <!-- Post Excerpt End -->

    <!-- Post Action Start -->
    <div class="post--action text-uppercase">
        <a href="blog-details.html" class="btn-link">Read More<i class="fa fa-long-arrow-right"></i></a>
    </div>
    <!-- Post Action End -->
</div>
<!-- Post Item End -->