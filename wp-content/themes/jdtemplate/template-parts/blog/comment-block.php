<!-- Comment Block Start -->
<div class="comment--block pb--60">
    <div class="comment--block-inner" >
        <div class="title text-uppercase pb--20">
            <h2 class="h4">
                <?php _e('Залиште коментар'); ?>
            </h2>
        </div>

        <form action="#" id="custom_comment_form">
            <input type="hidden" name="post_id" value="<?= get_the_ID(); ?>">
            <div class="row">
                <div class="col-xs-6 col-xs-12">
                    <div class="form-group">
                        <input type="text" name="name" placeholder="Ім'я"
                               class="form-control required" >
                    </div>
                </div>

                <div class="col-xs-6 col-xs-12">
                    <div class="form-group">
                        <input type="text" name="email"
                               placeholder="Поштова скринька"
                               class="form-control required email"
                        >
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="form-group">
                        <textarea name="message" placeholder="Коментар"
                                  class="form-control required"
                        ></textarea>
                    </div>
                </div>

                <div class="col-md-12 col-xs-12">
                    <button type="submit" class="btn btn-block btn-primary">
                        <?php _e('Опублікувати коментар'); ?>
                    </button>
                </div>
            </div>
        </form>
        <div id="form-message" style="display: none"></div>
    </div>
</div>
<!-- Comment Block End -->