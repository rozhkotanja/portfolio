<!-- Post Items Start -->
<div class="post--items">
<!-- Post Item Start -->
<div class="post--item">
    <div class="post--header clearfix">
        <!-- Post Date Start -->
        <div class="post--date ff--primary bg-primary">
            <p><a href="#">15 July 2017</a></p>
        </div>
        <!-- Post Date End -->

        <!-- Post Title Start -->
        <div class="post--title text-uppercase ov--h">
            <h3 class="h3 fw--400">
                <a href="blog-details.html" class="btn-link">Ut enim ad minim nostrud exerciation ullamco laboris nisi ut aliquip commodo consequat.</a>
            </h3>
        </div>
        <!-- Post Title End -->
    </div>

    <!-- Post Image Start -->
    <div class="post--img">
        <a href="blog-details.html"><img src="img/blog-img/post-item-04.jpg" alt=""></a>
    </div>
    <!-- Post Image End -->

    <!-- Post Meta Start -->
    <div class="post--meta ff--primary">
        <ul class="nav">
            <li>
                <i class="fa fa-user"></i>
                <a href="#">John Doe</a>
            </li>
            <li>
                <i class="fa fa-folder-o"></i>
                <a href="#">Design</a>
                <a href="#">Branding</a>
            </li>
            <li>
                <i class="fa fa-eye"></i>
                <span>12</span>
            </li>
        </ul>
    </div>
    <!-- Post Meta End -->

    <!-- Post Excerpt Start -->
    <div class="post--excerpt">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia soluta nam beatae commodi eligendi necessitatibus, obcaecati distinctio nemo odio inventore at expedita optio repudiandae sit, quibusdam molestiae quos ducimus qui? Deleniti eum aliquam facere debitis quaerat vero fuga, obcaecati deserunt libero laborum ducimus praesentium consectetur pariatur commodi suscipit veritatis odit nemo porro, nulla consequatur atque sunt eaque reprehenderit rerum. Autem.</p>
    </div>
    <!-- Post Excerpt End -->

    <!-- Post Action Start -->
    <div class="post--action text-uppercase">
        <a href="blog-details.html" class="btn-link">Read More<i class="fa fa-long-arrow-right"></i></a>
    </div>
    <!-- Post Action End -->
</div>
<!-- Post Item End -->