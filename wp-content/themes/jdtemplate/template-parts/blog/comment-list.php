<?php
    $comments = get_comments(array(
    'post_id' => get_the_ID()));
?>

<!-- Comment List Start -->
<div class="comment--list pb--60">
    <div class="title text-uppercase pb--20">
        <h2 class="h4">
            <?php echo __('Comments') . ' ('. count($comments) .')'; ?>
        </h2>
    </div>
    <ul class="comment--items nav">

        <?php   foreach($comments as $comment):  ?>

        <li style="width: 100%;">
            <!-- Comment Item Start -->
            <div class="comment--item clearfix">


                <div class="info ov--h">
                    <div class="header clearfix">
                        <div class="meta float--left">
                            <h3 class="name h4 fw--400">
                                <?php if($comment->comment_author){
                                    echo $comment->comment_author;
                                }?>
                            </h3>

                            <p class="date ff--primary">
                                <?php if($comment->comment_date){
                                    echo $comment->comment_date;
                                }?>
                            </p>
                        </div>


                    </div>

                    <div class="content">
                        <p>
                            <?php if($comment->comment_content){
                                echo $comment->comment_content;
                            }?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- Comment Item End -->

        </li>

        <?php endforeach; ?>


    </ul>
    <!-- Comment Items Start -->
</div>
<!-- Comment List End -->