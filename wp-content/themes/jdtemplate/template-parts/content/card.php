<!-- Service Block Start -->
<div class="service--block">
    <div class="icon">
        <i class="<?php echo $value['services_icon'] ?>"></i>
    </div>

    <div class="title">
        <h3 class="h4"> <?php echo $value['services_title'] ?></h3>
    </div>

    <div class="desc">
        <p><?php echo $value['services_description'] ?></p>
    </div>
</div>
<!-- Service Block End -->