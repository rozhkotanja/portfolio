<!-- Post Item Start -->
<div class="post--item">
    <div class="post--header clearfix">
        <!-- Post Date Start -->
        <div class="post--date ff--primary bg-primary">
            <p>
                <a href="javascript:void(0);">
                    <?php echo get_the_date('j F Y'); ?>
                </a>
            </p>
        </div>
        <!-- Post Date End -->

        <!-- Post Title Start -->
        <div class="post--title text-uppercase ov--h">
            <h3 class="h3 fw--400">
                <a href="<?php echo get_the_permalink(); ?>" class="btn-link">
                    <?php the_title(); ?>

                </a>
            </h3>
        </div>
        <!-- Post Title End -->
    </div>

    <!-- Post Image Start -->
    <div class="post--img">
        <a href="<?php echo get_the_permalink(); ?>">
            <img lazy-img-src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'posts-image'); ?>" alt="">
        </a>
    </div>
    <!-- Post Image End -->

    <!-- Post Meta Start -->
    <div class="post--meta ff--primary">
        <ul class="nav">
            <li style="display:none">
                <i class="fa fa-user"></i>
                <a href="#">John Doe</a>
            </li>
            <li>
                <i class="fa fa-folder-o"></i>

                <?php
                $terms = get_the_terms(get_the_ID(), 'categories_portfolio');
                if (!empty($terms)) {
                    foreach ($terms as $term) {
                        echo "<a href='" . get_home_url() . "/categories_portfolio/$term->slug'> $term->name</a>";
                    }
                }
                ?>

            </li>
            <li style="display:none">
                <i class="fa fa-eye"></i>
                <span>12</span>
            </li>
        </ul>
    </div>
    <!-- Post Meta End -->

    <!-- Post Excerpt Start -->
    <div class="post--excerpt">
        <p>
            <?php echo splited_string(get_the_content()); ?>

        </p>
    </div>
    <!-- Post Excerpt End -->

    <!-- Post Action Start -->
    <div class="post--action text-uppercase">
        <a href="<?php echo get_the_permalink(); ?>" class="btn-link">
            <?php echo _e('Читати далі') ?>
            <i class="fa fa-long-arrow-right"></i>
        </a>
    </div>
    <!-- Post Action End -->
</div>
<!-- Post Item End -->