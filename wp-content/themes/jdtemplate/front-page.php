<?php
get_header();
$data = get_post_meta(get_the_ID(), 'all_meta', true);//get_fields();
?>


    <!-- Banner Section Start -->
    <section id="banner" class="banner--section css-paralax"
             style="background-image: url(<?php echo $data['background_image']['url']; ?>);"
             data-overlay="0.8">
        <div class="vc--parent">
            <div class="vc--child">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <!-- Banner Content Start -->
                            <div class="banner--content text-center pt--160 pb--100">
                                <div class="title text-uppercase">
                                    <h2 class="h3 text-white">
                                    <span class="text-primary">
                                        <?php if ($data['_greeting_main']) : ?>
                                            <?php echo $data['_greeting_main']; ?>
                                        <?php endif ?>
                                    </span>
                                        <?php if ($data['greating']) : ?>
                                            <?php echo $data['greating']; ?>
                                        <?php endif ?>
                                    </h2>
                                </div>

                                <div class="name text-uppercase">
                                    <h3 class="h2 text-white">
                                        <?php if ($data['name']) : ?>
                                            <?php echo $data['name']; ?>
                                        <?php endif ?>
                                    </h3>
                                </div>

                                <div class="role fs--18 fw--700 text-white">
                                    <p>
                                        <?php if ($data['profession']) : ?>
                                            <?php echo $data['profession']; ?>
                                        <?php endif ?>
                                    </p>
                                </div>

                                <div class="desc">
                                    <p>
                                        <?php if ($data['description']) : ?>
                                            <?php echo $data['description']; ?>
                                        <?php endif ?>
                                    </p>
                                </div>

                                <div class="action">
                                    <?php if ($data['href_button_1']) : ?>
                                        <a href="<?php echo $data['href_button_1']; ?>"
                                           class="btn btn-default">
                                            <?php if ($data['button_1']) : ?>
                                                <?php echo $data['button_1']; ?>
                                            <?php endif ?>
                                        </a>
                                    <?php endif ?>
                                    <?php if ($data['href_button_2']) : ?>
                                        <a href="<?php echo $data['href_button_2'] ?>"
                                           class="btn btn-white">
                                            <?php if ($data['button_2']) : ?>
                                                <?php echo $data['button_2']; ?>
                                            <?php endif ?>
                                        </a>
                                    <?php endif ?>
                                </div>
                            </div>
                            <!-- Banner Content End -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Banner Section End -->
    <!-- About Section Start -->
    <section id="aboutme" class="section pt--100 pb--40">
        <div class="container">
            <!-- Section Title Start -->
            <div class="section--title pb--60 text-center">
                <h2 class="h2 text-uppercase">
                    <?php if ($data['about_me_text']) : ?>
                        <?php echo get_field('about_me_text'); ?>
                    <?php endif ?>
                </h2>
            </div>
            <!-- Section Title End -->

            <div class="row row--md-vc">
                <div class="col-md-6 pb--60">
                    <!-- Image Block Start -->
                    <div class="img--block">
                        <?php if ($data['about_me_main_image']) : ?>
                            <img lazy-img-src="<?php echo get_field('about_me_main_image'); ?>" alt="">
                        <?php endif ?>
                    </div>
                    <!-- Image Block End -->
                </div>

                <div class="col-md-6 pb--60">
                    <!-- About Block Start -->
                    <div class="about--block">
                        <div class="title">
                            <h2 class="h4 fw--700 text-uppercase">
                                <?php if ($data['personal_details']) : ?>
                                    <?php echo get_field('personal_details'); ?>
                                <?php endif ?>
                            </h2>
                        </div>

                        <div class="details pt--30 text-dark">
                            <?php if ($data['text_1']) : ?>
                                <p>
                                    <?php echo get_field('text_1'); ?>
                                </p>
                            <?php endif ?>
                        </div>

                        <div class="details pt--30">
                            <?php if ($data['text_2']) : ?>
                                <p>
                                    <?php echo get_field('text_2'); ?>
                                </p>
                            <?php endif ?>
                        </div>

                        <div class="info pt--50">
                            <table class="ff--primary">
                                <tbody>
                                <?php if (!empty($data['about_me_contacts'])) : ?>
                                    <?php foreach ($data['about_me_contacts'] as $key => $value) : ?>
                                        <tr>
                                            <th>
                                                <i class="<?php echo $value['about_me_icons'] ?>"></i>
                                                <span>
                                                    <?php echo $value['about_me_title'] ?>
                                                </span>
                                            </th>
                                            <td>
                                                <?php echo $value['about_me_info'] ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif ?>
                                </tbody>
                            </table>
                        </div>

                        <div class="social text-dark pt--30">
                            <ul class="nav">
                                <?php  if (!empty(get_field('footer', 'option')['socials'])) : ?>
                                    <?php foreach (
                                        get_field('footer', 'option')['socials']
                                        as $key => $value) :
                                        ?>
                                        <li>
                                            <a title="<?php echo $value['title']; ?>"
                                               target="_blank"
                                               href="<?php echo $value['link']; ?>">
                                            <span class="
                                                <?php echo $value['icon']; ?>">
                                            </span>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                <?php endif ?>
                            </ul>
                        </div>

                        <!-- <div class="action pt--60">
                            <a href="#" class="btn btn-default">Not Need</a>
                        </div> -->

                    </div>
                    <!-- About Block End -->
                </div>
            </div>
        </div>
    </section>
    <!-- About Section End -->

    <!-- Services Section Start -->
    <section id="services" class="section pt--100 pb--40 bg-light">
        <div class="container">
            <!-- Section Title Start -->
            <div class="section--title pb--60 text-center">
                <h2 class="h2 text-uppercase">
                    <?php if ($data['services_main_title']) : ?>
                        <?php echo $data['services_main_title'] ?>
                    <?php endif ?>
                </h2>
            </div>
            <!-- Section Title End -->

            <div class="row AdjustRow" data-scroll-reveal="group">
                <?php if (!empty($data['services_step'])) : ?>
                    <?php foreach ($data['services_step'] as $key => $value) : ?>
                        <div class="col-md-3 col-xs-6 col-xxs-12 pb--60">
                            <?php include('template-parts/content/card.php'); ?>
                        </div>
                    <?php endforeach; ?>
                <?php endif ?>
            </div>
        </div>
    </section>
    <!-- Services Section End -->

    <!-- Portfolio Section Start -->
    <section id="portfolio" class="section pt--100 pb--40">
        <div class="container">
            <!-- Section Title Start -->
            <div class="section--title pb--60 text-center">
                <h2 class="h2 text-uppercase">
                    <?php echo $data['portfolio']['portfolio_title']; ?>
                </h2>
            </div>
            <!-- Section Title End -->

            <!-- Portfolio Filter Menu Start -->
            <div class="portfolio--filter-menu pb--50">
                <ul class="nav">
                    <li data-target="*" class="active"><?php _e('Всі роботи'); ?></li>
                    <?php
                    $terms = get_terms([
                        'taxonomy' => 'categories_portfolio',
                        'hide_empty' => true,
                    ]);

                    foreach ($terms as $term):
                        ?>
                        <li data-target="<?php echo $term->slug; ?>">
                            <?php echo $term->name; ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <!-- Portfolio Filter Menu End -->

            <!-- Portfolio Items Start -->
            <div class="portfolio--items row MasonryRow pb--30">
                <?php
                $args = array(
                    'posts_per_page' => -1,
                    'post_type' => 'portfolio',
                );

                $query = new WP_Query($args);
                if ($query->have_posts()) {
                    while ($query->have_posts()) {
                        $query->the_post();
                        ?>


                        <div class="col-md-3 col-xs-6 col-xxs-12 pb--30"
                             data-cat="<?php echo get_the_terms(get_the_ID(),
                                 'categories_portfolio')[0]->slug ?>">
                            <!-- Portfolio Item Start -->
                            <div class="portfolio--item">
                                <?php $image_isset = get_the_post_thumbnail_url(get_the_ID(),
                                    'portfolio-img');
                                if (!empty($image_isset)): ?>
                                    <img src="<?php echo $image_isset; ?>" alt="">
                                <?php endif; ?>
                                <a href="<?php echo get_permalink(); ?>" class="caption">
                                    <div class="vc--parent">
                                        <div class="vc--child">
                                            <h3 class="h6">
                                                <?php echo get_the_title(); ?>
                                            </h3>

                                            <p><?php echo the_excerpt(); ?></p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <!-- Portfolio Item End -->
                        </div>

                        <?php
                    }
                } else {
                    // Постов не найдено
                }
                ?>

                <!-- Portfolio Items End -->
            </div>
    </section>
    <!-- Portfolio Section End -->

    <!-- Call To Action Start -->
    <section class="section css-paralax pt--100 pb--100"
             data-overlay="0.5"
             style="background-image: url(<?php echo $data['image_question']['url']; ?>);">
        <div class="container">
            <!-- Call To Action Block Start -->
            <div class="cta--block text-center">
                <div class="title">
                    <h2 class="h2 text-white">
                        <?php if ($data['question_hire']['question']) : ?>
                            <?php echo $data['question_hire']['question'] ?>
                        <?php endif ?>
                    </h2>
                </div>

                <div class="content fs--20 pt--10 text-white">
                    <p>
                        <?php if ($data['question_hire']['question_description']) : ?>
                            <?php echo $data['question_hire']['question_description'] ?>
                        <?php endif ?>
                    </p>
                </div>

                <div class="action pt--20">
                    <?php if ($data['question_hire']['question_link']) : ?>
                        <a href="<?php echo $data['question_hire']['question_link'] ?> "
                           class="btn btn-white">
                            <?php if ($data['question_hire']['question_button_text']) : ?>
                                <?php echo $data['question_hire']['question_button_text'] ?>
                            <?php endif ?>
                        </a>
                    <?php endif ?>

                </div>
            </div>
            <!-- Call To Action Block End -->
        </div>
    </section>
    <!-- Call To Action End -->

    <!-- Resume Section Start -->
    <section id="resume" class="section pt--100 pb--100">
        <div class="container">
            <!-- Section Title Start -->
            <div class="section--title pb--60 text-center">
                <h2 class="h2 text-uppercase">
                    <?php if ($data['my_resume']['my_resume_title']) : ?>
                        <?php echo $data['my_resume']['my_resume_title'] ?>
                    <?php endif ?>
                </h2>
            </div>
            <!-- Section Title End -->

            <!-- Timeline Block Start -->
            <div class="timeline--block">
                <div class="title">
                    <h3 class="h5">
                        <?php if ($data['my_resume']['education_title']) : ?>
                            <?php echo $data['my_resume']['education_title'] ?>
                        <?php endif ?>
                    </h3>
                </div>

                <div class="sub-title">
                    <p class="fs--16">
                        <?php if ($data['my_resume']['education_text']) : ?>
                            <?php echo $data['my_resume']['education_text'] ?>
                        <?php endif ?>
                    </p>
                </div>

                <!-- Timeline Items Start -->
                <ul class="timeline--items nav">
                    <?php if (!empty($data['my_resume']['education_step'])) : ?>
                        <?php foreach ($data['my_resume']['education_step'] as $key => $value) : ?>
                            <li>
                                <!-- Timeline Item Start -->
                                <div class="timeline--item">
                                    <div class="icon bg-default">
                                        <i class="fa fa-graduation-cap"></i>
                                    </div>

                                    <table class="table" data-scroll-reveal="bottom">
                                        <tr>
                                            <td>
                                                <p class="date bg-primary">
                                                    <?php echo $value['education_date'] ?>
                                                </p>
                                                <div class="bottom">
                                                    <h4 class="name h6 fw--700">
                                                        <?php echo
                                                        $value['education_academy'] ?>
                                                    </h4>
                                                    <p class="institute ff--primary">
                                                        <?php echo
                                                        $value['education_master'] ?>
                                                    </p>
                                                    <p class="location fs--12">
                                                        <?php echo
                                                        $value['education_city'] ?>
                                                    </p>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="content">
                                                    <p>
                                                        <?php echo
                                                        $value['education_description'] ?>
                                                    </p>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!-- Timeline Item End -->
                            </li>
                        <?php endforeach; ?>
                    <?php endif ?>
                </ul>
                <!-- Timeline Items End -->
            </div>
            <!-- Timeline Block End -->
        </div>
    </section>
    <!-- Resume Section End -->

    <!-- Experience Section Start -->
    <section class="section pt--100 pb--100 bg-light">
        <div class="container">
            <!-- Timeline Block Start -->
            <div class="timeline--block">
                <div class="title">
                    <?php if ($data['experience']['experience_title']) : ?>
                        <h3 class="h5">
                            <?php echo $data['experience']['experience_title'] ?>
                        </h3>
                    <?php endif ?>
                </div>

                <div class="sub-title">
                    <p class="fs--16">
                        <?php if ($data['experience']['experience_text']) : ?>
                            <?php echo $data['experience']['experience_text'] ?>
                        <?php endif ?>
                    </p>
                </div>

                <!-- Timeline Items Start -->
                <ul class="timeline--items nav">
                    <?php if (!empty($data['experience']['experience_step'])) : ?>
                        <?php foreach ($data['experience']['experience_step'] as $key => $value) : ?>
                            <li>
                                <!-- Timeline Item Start -->
                                <div class="timeline--item">
                                    <div class="icon bg-light">
                                        <i class="fa fa-briefcase"></i>
                                    </div>

                                    <table class="table" data-scroll-reveal="bottom">
                                        <tr>
                                            <td>
                                                <p class="date bg-primary">
                                                    <?php echo
                                                    $value['experience_date'] ?>
                                                </p>
                                                <div class="bottom">
                                                    <h4 class="name h6 fw--700">
                                                        <?php echo
                                                        $value['experience_company_name'] ?>
                                                    </h4>
                                                    <p class="institute ff--primary">
                                                        <?php echo
                                                        $value['experience_position'] ?>
                                                    </p>
                                                    <p class="location fs--12">
                                                        <?php echo
                                                        $value['experience_city'] ?>
                                                    </p>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="content">
                                                    <p>
                                                        <?php echo
                                                        $value['experience_description'] ?>
                                                    </p>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!-- Timeline Item End -->
                            </li>

                        <?php endforeach ?>
                    <?php endif ?>
                </ul>
                <!-- Timeline Items End -->
            </div>
            <!-- Timeline Block End -->
        </div>
    </section>
    <!-- Experience Section End -->

    <!-- Skills Section Start -->
    <section id="skills"
             class="section css-paralax pt--100 pb--40"
             data-bg-img="<?php if ($data['my_skills']['skills_image']['url']) {
                 echo $data['my_skills']['skills_image']['url'];
             }
             ?>"
             data-overlay="0.7">
        <div class="container">
            <!-- Section Title Start -->
            <div class="section--title pb--60 text-center">
                <h2 class="h2 text-white text-uppercase">
                    <?php if ($data['my_skills']['skills_title']) {
                        echo $data['my_skills']['skills_title'];
                    }
                    ?>
                </h2>
            </div>
            <!-- Section Title End -->

            <div class="row">

                <?php if (!empty($data['my_skills']['skills_block'])) : ?>
                    <?php foreach ($data['my_skills']['skills_block'] as $key => $value) : ?>

                        <div class="col-md-6 pb--60">
                            <!-- Skills Items Start -->
                            <div class="skills--items">
                                <div class="skill--title text-uppercase">
                                    <h3 class="h4 text-white">
                                        <?php if ($value['skils_block_title']) {
                                            echo $value['skils_block_title'];
                                        } ?>
                                    </h3>
                                </div>
                                <?php if (!empty($value['skils_block_item'])) : ?>
                                    <?php foreach ($value['skils_block_item'] as $key => $value_item) : ?>
                                        <!-- Skill Item Start -->
                                        <div class="skill--item">
                                            <h4 class="h6 fw--400 text-white text-uppercase clearfix">
                                                <span class="skill--text">
                                                    <?php if ($value_item['item_title']) {
                                                        echo $value_item['item_title'];
                                                    } ?>
                                                </span>
                                                <span class="skill--count float--right">
                                                    <?php if ($value_item['item_percent']) {
                                                        echo $value_item['item_percent'] . '%';
                                                    } ?>
                                                </span>
                                            </h4>

                                            <p class="skill--progress" style="width:
                                            <?php if ($value_item['item_percent']) {
                                                echo $value_item['item_percent'] . '%';
                                            } ?>;"
                                               data-trigger="progress_bar"></p>
                                        </div>
                                        <!-- Skill Item End -->

                                    <?php endforeach ?>
                                <?php endif ?>

                            </div>
                            <!-- Skills Items End -->
                        </div>
                    <?php endforeach ?>
                <?php endif ?>

            </div>
        </div>
    </section>
    <!-- Skills Section End -->

    <!-- Pricing Section Start -->
    <section id="pricing" class="section pt--100 pb--40 bg-light">
        <div class="container">
            <!-- Section Title Start -->
            <div class="section--title pb--60 text-center">
                <h2 class="h2 text-uppercase">
                    <?php echo $data['pricing_plan_title'];?>
                </h2>
            </div>
            <!-- Section Title End -->
            <div class="row AdjustRow">
                <?php if (!empty($data['pricing_plan'])) : ?>
                    <?php foreach ($data['pricing_plan'] as $key => $value) : ?>
                        <div class="col-md-4 col-xs-12 pb--60">
                            <!-- Pricing Item Start -->
                            <div class="pricing--item">
                                <div class="pricing--header bg-primary">
                                    <div class="plan">
                                        <h3 class="h3 text-primary">
                                            <?php echo $value['pricing_plan_name']; ?>
                                        </h3>
                                    </div>
                                    <div class="price ff--primary">
                                        <p>
                                            <?php echo $value['price_form']; ?>
                                            <span>
                                            <?php echo $value['price']; ?>
                                        </span>
                                        </p>
                                        </p>
                                    </div>
                                </div>
                                <div class="pricing--features fs--16 ff--primary">
                                    <ul class="nav">
                                        <?php if (!empty($value['_conditions'])) : ?>
                                            <?php foreach ($value['_conditions'] as $key => $condition) : ?>
                                                <li>
                                                    <?php echo
                                                    $condition['_conditions_description']; ?>
                                                </li>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </ul>
                                </div>
                                <div class="pricing--action">
                                    <a href="<?php echo $value['order_link']; ?>"
                                       class="btn btn-primary">
                                        <?php echo $value['order_button']; ?>
                                    </a>
                                </div>
                            </div>
                            <!-- Pricing Item End -->
                        </div>
                    <?php endforeach ?>
                <?php endif ?>
            </div>
        </div>
    </section>
    <!-- Pricing Section End -->


    <!-- Call To Action Start -->
    <section class="section css-paralax pt--100 pb--100"
             data-overlay="0.5"
             style="background-image: url(<?php echo $data['image_question']['url']; ?>);">
        <div class="container">
            <!-- Call To Action Block Start -->
            <div class="cta--block text-center" style="display: none">
                <div class="title">
                    <h2 class="h2 text-white">
                        <?php if ($data['question_hire']['question']) : ?>
                            <?php echo $data['question_hire']['question'] ?>
                        <?php endif ?>
                    </h2>
                </div>
            </div>
            <!-- Call To Action Block End -->
        </div>
    </section>
    <!-- Call To Action End -->


    <!-- Team Section Start -->
    <section id="team" class="section pt--100 pb--40" style="display: none">
        <div class="container">
            <!-- Section Title Start -->
            <div class="section--title pb--60 text-center">
                <h2 class="h2 text-uppercase">Meet My Team</h2>
            </div>
            <!-- Section Title End -->

            <div class="row AdjustRow">
                <div class="col-md-4 col-xs-12 pb--60">
                    <!-- Team Member Item Start -->
                    <div class="team--member" data-scroll-reveal="bottom">
                        <div class="img">
                            <img lazy-img-src="<?php echo get_template_directory_uri(); ?>/img/team-img/member-01.jpg"
                                 alt="">
                        </div>

                        <div class="caption">
                            <div class="vc--parent">
                                <div class="vc--child">
                                    <div class="name">
                                        <h3 class="h3 fw--400">Mike Jones</h3>
                                    </div>

                                    <div class="company ff--primary">
                                        <p>ThemeLooks</p>
                                    </div>


                                    <div class="social">
                                        <ul class="nav">
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Team Member Item End -->
                </div>

                <div class="col-md-4 col-xs-12 pb--60">
                    <!-- Team Member Item Start -->
                    <div class="team--member" data-scroll-reveal="bottom">
                        <div class="img">
                            <img lazy-img-src="<?php echo get_template_directory_uri(); ?>/img/team-img/member-02.jpg"
                                 alt="">
                        </div>

                        <div class="caption">
                            <div class="vc--parent">
                                <div class="vc--child">
                                    <div class="name">
                                        <h3 class="h3 fw--400">Jack Roberts</h3>
                                    </div>

                                    <div class="company ff--primary">
                                        <p>ThemeLooks</p>
                                    </div>

                                    <div class="social">
                                        <ul class="nav">
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Team Member Item End -->
                </div>

                <div class="col-md-4 col-xs-12 pb--60">
                    <!-- Team Member Item Start -->
                    <div class="team--member" data-scroll-reveal="bottom">
                        <div class="img">
                            <img lazy-img-src="<?php echo get_template_directory_uri(); ?>/img/team-img/member-03.jpg"
                                 alt="">
                        </div>

                        <div class="caption">
                            <div class="vc--parent">
                                <div class="vc--child">
                                    <div class="name">
                                        <h3 class="h3 fw--400">Dennis Murray</h3>
                                    </div>

                                    <div class="company ff--primary">
                                        <p>ThemeLooks</p>
                                    </div>

                                    <div class="social">
                                        <ul class="nav">
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Team Member Item End -->
                </div>
            </div>
        </div>
    </section>
    <!-- Team Section End -->

    <!-- Blog Section Start -->
    <section id="blog" class="section pt--100 pb--40 bg-light">
        <div class="container">
            <!-- Section Title Start -->
            <div class="section--title pb--60 text-center">
                <h2 class="h2 text-uppercase">
                    <?php if ($data['blog_&_news']['blog_title']) : ?>
                        <?php echo $data['blog_&_news']['blog_title']; ?>
                    <?php endif ?>
                </h2>
            </div>
            <!-- Section Title End -->

            <div class="row row_flex">

                <?php
                $args_blog = array(
                    'post_type' => 'blog',
                    'posts_per_page' => 3
                );

                $query_blogs = new WP_Query($args_blog);


                if ($query_blogs->have_posts()) {
                    while ($query_blogs->have_posts()) {
                        $query_blogs->the_post();

                        ?>
                        <div class="col-md-4 col-xs-12 pb--60">
                            <!-- Post Item Start -->

                            <div class="post--item">
                                <!-- Post Image Start -->
                                <div class="post--img">
                                    <?php $image_blog = get_the_post_thumbnail_url(get_the_ID(), 'portfolio-img');
                                    if (!empty($image_blog)): ?>
                                        <a href="<?php echo get_permalink(); ?>"><img
                                                    lazy-img-src="<?php echo $image_blog; ?>"
                                                    alt="">
                                        </a>
                                    <?php endif; ?>

                                    <a href="javascript:void(0);" class="date">
                                        <?php echo get_the_date('j F Y'); ?>
                                    </a>
                                </div>
                                <!-- Post Image End -->

                                <!-- Post Title Start -->
                                <div class="post--title text-uppercase">
                                    <h3 class="h3 fs--22">
                                        <a href="<?php echo get_permalink(); ?>" class="btn-link">
                                            <?php echo get_the_title(); ?>
                                        </a>
                                    </h3>
                                </div>
                                <!-- Post Title End -->

                                <!-- Post Excerpt Start -->
                                <div class="post--excerpt">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi
                                        dignissimos
                                        molestiae
                                        voluptates ullam dolor mollitia quos commodi...</p>
                                </div>
                                <!-- Post Excerpt End -->

                                <!-- Post Action Start -->
                                <div class="post--action text-uppercase">
                                    <a href="blog-details.html" class="btn-link">Read More<i
                                                class="fa fa-long-arrow-right"></i></a>
                                </div>
                            </div>
                            <!-- Post Item End -->
                        </div>
                        <?php
                    }
                } else {
                    // Постов не найдено
                }
                ?>
                <!-- Post Action End -->


            </div>

            <!-- Section Footer Start -->
            <div class="section--footer text-center pb--60">
                <a href="<?php if ($data['blog_&_news']['blog_button_url']) {
                    echo $data['blog_&_news']['blog_button_url'];
                } ?>" class="btn btn-primary">

                    <?php if ($data['blog_&_news']['blog_button']) : ?>
                        <?php echo $data['blog_&_news']['blog_button']; ?>
                    <?php endif ?>
                </a>
            </div>
            <!-- Section Footer End -->
        </div>
    </section>
    <!-- Blog Section End -->

    <!-- Testimonial Section Start -->
    <section id="testimonial"
             class="section css-paralax pt--90 pb--90"
             style="background-image: url(<?php echo $data['image-reviews']['url']; ?>);"
             data-overlay="0.5">
        <div class="container">
            <!-- Testimonial Slider Start -->
            <div class="testimonial--slider owl-carousel" data-owl-dots="true">
                <?php if (!empty($data['reviews_info'])): ?>
                    <?php foreach ($data['reviews_info'] as $key => $value) : ?>
                        <!-- Testimonial Item Start -->
                        <div class="testimonial--item text-white">
                            <div class="icon">
                                <i class="fa fa-comments"></i>
                            </div>
                            <blockquote>
                                <p>
                                    <?php echo $value['reviews_text'] ?>
                                </p>
                                <footer>
                                    <?php echo $value['reviews_signature'] ?>
                                </footer>
                            </blockquote>
                        </div>
                        <!-- Testimonial Item End -->
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
    </section>
    <!-- Testimonial Section End -->

    <!-- Contact Section Start -->
    <section id="contact" class="section pt--100 pb--40 bg-light">
        <div class="container">
            <!-- Section Title Start -->
            <div class="section--title pb--60 text-center">
                <h2 class="h2 text-uppercase">
                    <?php if (get_field('contact', 'option')['contact_title']): ?>
                        <?php echo get_field('contact', 'option')['contact_title'] ?>
                    <?php endif; ?>
                </h2>
            </div>
            <!-- Section Title End -->

            <div class="row AdjustRow pb--10">

                <?php if (!empty(get_field('contact', 'option')['contacts_info'])): ?>
                    <?php foreach (get_field('contact', 'option')['contacts_info'] as $key => $value) : ?>
                        <div class="col-md-4 col-xs-12 pb--40">
                            <!-- Contact Info Block Start -->
                            <div class="contact--info-block" data-scroll-reveal="bottom">
                                <div class="icon">
                                    <i class="<?php echo
                                    $value['contact_icon']; ?>">
                                    </i>
                                </div>
                                <div class="title text-uppercase">
                                    <h3 class="h4">
                                        <?php echo $value['contact_title']; ?>
                                    </h3>
                                </div>
                                <div class="desc">
                                    <p>
                                        <a href="<?php echo
                                        $value['contact_href']; ?>" class="btn-link">
                                            <?php echo $value['contact_description']; ?>
                                        </a>,
                                        <a href="<?php echo
                                        $value['contact_href_2']; ?>" class="btn-link">
                                            <?php echo $value['contact_description_2']; ?>
                                        </a>
                                    </p>
                                </div>
                            </div>
                            <!-- Contact Info Block End -->
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>

            <!-- Contact Form Start -->
            <div class="contact--form pb--60" data-form="ajax">
                <div class="title text-center text-uppercase pb--30">
                    <h3 class="h4">
                        <?php echo get_field('contact', 'option')['contact_form_title'];?>
                    </h3>
                </div>

                <?php echo get_field('contact', 'option')['contact_form'];?>

            </div>
            <!-- Contact Form End -->
        </div>
    </section>
    <!-- Contact Section End -->


<?php get_footer(); ?>